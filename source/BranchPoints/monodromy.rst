========================
Monodromy representation
========================


In this section, you find the code used for computations with monodromy representations. 
The file :download:`auxiliary_functions_monodromy.jl <auxiliary_functions_monodromy.jl>` is needed for all other files, 
and contains auxiliary funtions used for both internal functioning and different output modes. 

Monodromy representations of degree 3, genus 1 and degree 4, genus 3
--------------------------------------------------------------------

:download:`monodromy_list.jl <monodromy_list.jl>`


A monodromy representation of degree d is a list of permutations :math:`(\tau_1, \dots, \tau_i)` in :math:`\mathbb{S}_d`
such that the :math:`\tau_i` 
act transitively on :math:`(1, \dots, d)` and :math:`\tau_i \circ \cdots \circ \tau_1 = id`. The group :math:`\mathbb{S}_d` acts on the set of 
monodromy representations by conjugation. 
For readability of the following examples, we alias the transpositions of degree 4 in the following way:

.. code-block:: julia

                a = Perm([2,1,3,4])
                b = Perm([3,2,1,4])
                c = Perm([4,2,3,1])
                d = Perm([1,3,2,4])
                e = Perm([1,4,3,2])
                f = Perm([1,2,4,3])
                id = Perm([1,2,3,4])



We count monodromy representations by counting equivalence classes of monodromy representations under this conjugation. We say that 
the *canonical representative* of such a class is the lexicographically smallest element. Our code computes the canonical representative
of a monodromy representation of degree 3 and 4:


.. code-block:: julia

    # Example in degree 3: Canonical representative in the class of (12)(23)(13)(13)(23)(12)
    monod_d3 = [Perm([2,1,3]),Perm([1,3,2]),Perm([3,2,1]),Perm([3,2,1]),Perm([1,3,2]),Perm([2,1,3])]
    canonical_monodromy_representation_degree_3(monod)
    # Example in degree 4: Canonical representative in the class of (12)(34)(24)(13)(24)(13)(13)(13)(13)(13)(12)(34)
    monod_d4 = [a, f, e, b, e, b, b, b, b, b, a, f]
    canonical_monodromy_representation_degree_4(monod)


Now we can list canonical representatives for all monodromy representations of degree 3, using

.. code-block:: julia

    list_deg_3 = all_monodromy_reps_degree_3()


We can further identify lists of monodromy representations of degree 3 with the notation introduced in Table 1 of our paper, using

.. code-block:: julia

    current_notation = map_to_current_notation_cubics(list_deg_3)


Finally, all_monodromy_reps_degree_4() returns the vector of canonical monodromy representations of degree 4. It has size 7528620 and takes approximately 6.5 hours
and requires some free internal memory.


Real monodromy 
--------------

:download:`real_monodromy.jl <real_monodromy.jl>`


To show that a monodromy representation is a real, we apply the algorithm of [1] Section 3.3. While the code in the above Section
only works for degrees 3 and 4, the following code works for all degrees. 

Cadorets algorithm checks whether a monodromy representation is real for a fixed number of real branch points and number of pairs of complex conjugate branch points under
a specified involution

.. code-block:: julia

    # Example in degree 3
    real = 6
    pairs = 0
     is_real_monodromy_representation(monod_d3, Perm([2,1,3]), real, pairs)


.. code-block:: julia

    # Real example in degree 4
    real = 12
    pairs = 0
     is_real_monodromy_representation(monod_d4, a*f, real, pairs)

.. code-block:: julia

    # Example in degree 4
    real = 8
    pairs = 2
     is_real_monodromy_representation(monod_d4, a, real, pairs)


We can also compute which elements of a list are real for a fixed number of real branch points and number of pairs of complex conjugate branch points under
a specified involution:

.. code-block:: julia

    compute_real_monodromy_list(list_deg_3, Perm([1,2,3]), 6,0)
    compute_real_monodromy_list(list_deg_3, Perm([2,1,3]), 6,0)


We used this method in our paper to prove Proposition 3.6 and Theorem 3.7, combined with the algorithms of the previous section.


References
----------

[1] A. Cadoret:Counting real Galois covers of the projective line, Pacific J. Math. 219 (2005) 53–81
