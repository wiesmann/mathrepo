
using Oscar
using auxiliary_functions_monodromy.jl

#monodromy_list_degree_4
id = Perm([1,2,3,4])

## checks, whether a length 12 Vector of permutations is a monodromy representation of degree 4, i.e. acts transitively on 1,2,3,4
function is_monodromy_representation(monod::Vector{Perm{Int64}}) 
    a = Perm([2,1,3,4])
    b = Perm([3,2,1,4])
    c = Perm([4,2,3,1])
    d = Perm([1,3,2,4])
    e = Perm([1,4,3,2])
    f = Perm([1,2,4,3])
    three = false
    four = false
    actual_transposition = false
    for i in 1:length(monod)
        if monod[i] !=f && monod[i]!=a
            actual_transposition=true
        end
        if monod[i] == f
            three = true
            four = true
        elseif monod[i] == b||monod[i]==d
            three = true
        elseif monod[i] == c ||monod[i]==e
            four = true
        end
        if three && four && actual_transposition
            return true
        end
    end
    return false
end

## checks, whether a length 6 Vector of permutations is a monodromy representation of degree 3, i.e. acts transitively on 1,2,3
function is_monodromy_representation_degree_3(monod::Vector{Perm{Int64}})
    a = Perm([2,1,3])
    b = Perm([3,2,1])
    c = Perm([1,3,2])
    three = false
    for i in 1:length(monod)
        if monod[i] == b || monod[i] ==c
            return true
        end
    end
    return false
end

# returns a list of monodromy representations of degree 3 and genus 1, containing the canonical representative for each equivalence class of 
# monodromy representations 
function all_monodromy_reps_degree_3()
    a = Perm([2,1,3])
    b = Perm([3,2,1])
    c = Perm([1,3,2])
    Transpositions = [a,b,c]
    already_constructed=[a]
    full_monodromy = Vector{Vector{Perm{Int64}}}([])
    all_monodromy_reps_internal_degree_3(already_constructed,Transpositions,1,6,full_monodromy,1)
    return full_monodromy
end

# internal recursion for all_monodromy_reps_degree_3
function all_monodromy_reps_internal_degree_3(already_constructed::Vector{Perm{Int64}}, transpos::Vector{Perm{Int64}},current_length::Int64, wanted_length::Int64, full_monodromy::Vector{Vector{Perm{Int64}}})
    upper_bound = min(current_length+1,length(transpos))
    for i in 1:upper_bound
        already_constructed_temp =deepcopy(already_constructed)
        push!(already_constructed_temp,transpos[i])
        current_length = length(already_constructed_temp)
        if current_length == wanted_length-1
            out = monodromy_length_minus_1(already_constructed_temp)
            println(out)
            println( canonical_monodromy_representation_degree_3(out[2]))
            println(out[2] == canonical_monodromy_representation_degree_3(out[2]))
            if out[1] && is_monodromy_representation_degree_3(out[2]) && out[2] == canonical_monodromy_representation_degree_3(out[2])
                println(out[2])
                push!(full_monodromy,out[2])
            end
        elseif current_length > wanted_length-1
            return "text"
        else
            all_monodromy_reps_internal_degree_3(already_constructed_temp, transpos,current_length, wanted_length, full_monodromy,count)
        end
    end
end

# returns a list of monodromy representations of degree 4 and genus 3, containing the canonical representative for each equivalence class of 
# monodromy representations 
function all_monodromy_reps_degree_4()
    a = Perm([2,1,3,4])
    b = Perm([3,2,1,4])
    c = Perm([4,2,3,1])
    d = Perm([1,3,2,4])
    e = Perm([1,4,3,2])
    f = Perm([1,2,4,3])    
    Transpositions = [a,f,b,e,c,d]
    already_constructed =[a]
    full_monodromy = Vector{Vector{Perm{Int64}}}([])
    not_canonical = Vector{Vector{Perm{Int64}}}([])
    all_monodromy_reps_internal(already_constructed,Transpositions,1,12,full_monodromy)
    return full_monodromy
end



# internal recursion for all_monodromy_reps_degree_4
function all_monodromy_reps_internal(already_constructed::Vector{Perm{Int64}}, transpos::Vector{Perm{Int64}},current_length::Int64, wanted_length::Int64, full_monodromy::Vector{Vector{Perm{Int64}}})
    if current_length == 1
        upper_bound = 3
    else
        upper_bound = 6
    end

    for i in 1:upper_bound
        already_constructed_temp =deepcopy(already_constructed)
        push!(already_constructed_temp,transpos[i])
        current_length = length(already_constructed_temp)
        if current_length == wanted_length-1
            out = monodromy_length_minus_1(already_constructed_temp)
            if out[1] && is_monodromy_representation(out[2]) && out[2] == canonical_monodromy_representation_degree_4(out[2])
                push!(full_monodromy,out[2])
            end
        elseif current_length > wanted_length-1
            return "text"
        else
            all_monodromy_reps_internal(already_constructed_temp, transpos,current_length, wanted_length, full_monodromy)
        end
    end
end

# internal function for the last element in each monodromy representation of the list of degree 4, ensuring an added element composes to the
# identity
function monodromy_length_minus_1(already_constructed::Vector{Perm{Int64}})
    m = already_constructed[1]
    a = Perm([2,1,3,4])
    for i in 2:length(already_constructed)
        m = m*already_constructed[i]
    end
    c = permtype(m)
    if c[1] == 2
        if c[2] == 1
            n = [inv(m)]
                return (true,cat(already_constructed,n,dims=1))
            end
        end
    return (false,Perm([1,2,3,4]))             
end
