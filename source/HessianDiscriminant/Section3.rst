=========
Section 3
=========

3.1. Computing invariants for cubics of higher rank
===================================================
Below is the computation (in Macaulay2) for Salmon's invariants for general rank 6 cubics.

We write :math:`\verb|u|=\varepsilon^{-1},\verb|l0|=\lambda_0,\verb|l1|=\lambda_1,\verb|l2|=\lambda_2,\verb|m|=\mu`.

.. code-block:: macaulay2

		K=QQ;
		S=frac(K[l0,l1,l2,m]);
		R=S[u];
		a=l0^(-3)*u^3;
		b=l1^(-3)*u^3;
		c=l2^(-3)*u^3;
		d=(u-m);
		e=u;
		p=a+b+c+d+e;
		q=a*b+a*c+a*d+a*e+b*c+b*d+b*e+c*d+c*e+d*e;
		r=c*d*e+b*d*e+b*c*e+b*c*d+a*d*e+a*c*e+a*c*d+a*b*e+a*b*d+a*b*c;
		s=b*c*d*e+c*d*e*a+d*e*a*b+e*a*b*c+a*b*c*d;
		t=a*b*c*d*e;
		A=s^2-4*r*t;
		B=t^3*p;
		C=t^4*s;
		D=t^6*q;
		E=t^8;
		--Since we want the limit as u goes to infinity, it suffices to look at the leading terms
		--We obtain the following point in weighted projective space P(1,2,3,4,5)
		Q=[leadTerm A, leadTerm B, leadTerm C, leadTerm D, leadTerm E]
		


The final output is

.. code-block:: macaulay2
		
		            3      3      3    2       3  3     3  3     3  3    
                       - 4l0  - 4l1  - 4l2  + m  18  l0 l1  + l0 l2  + l1 l2  36 
                o19 = [-------------------------u  , ------------------------u  ,
                                 6  6  6                     12  12  12          
                               l0 l1 l2                    l0  l1  l2            
                      -------------------------------------------------------------------------
                                         3     3     3
                            2      54  l0  + l1  + l2  72        1      88
                      ------------u  , ---------------u  , ------------u  ]
                        15  15  15         21  21  21        24  24  24
                      l0  l1  l2         l0  l1  l2        l0  l1  l2
      
After clearing denominators and letting :math:`\varepsilon \to 0`, this becomes

.. math:: [\mu^2-4(\lambda_0^3+\lambda_1^3+\lambda_2^3): \lambda_0^3\lambda_1^3+\lambda_0^3\lambda_2^3+\lambda_1^3\lambda_2^3: 2\lambda_0^3\lambda_1^3\lambda_2^3: \lambda_0^3\lambda_1^3\lambda_2^3(\lambda_0^3+\lambda_1^3+\lambda_2^3): 0]

as a point in :math:`\mathbb{P}(1,2,3,4,5)`.
