====================================
Operations for D-Algebraic Functions
====================================

| This page contains auxiliary files to the paper:
| Bertrand Teguia Tabuguia: Operations for D-Algebraic Functions
| ARXIV: https://arxiv.org/abs/2304.09675  CODE: https://mathrepo.mis.mpg.de/OperationsForDAlgebraicFunctions

ABSTRACT: A function is differentially algebraic (or simply D-algebraic) if there is 
a polynomial relationship between some of its derivatives and the indeterminate variable. 
Many functions in the sciences, such as Mathieu functions, the Weierstrass elliptic functions, 
and holonomic or D-finite functions are D-algebraic. These functions form a field, and are 
closed under composition, taking functional inverse, and derivation. We present implementation 
for each underlying operation. We also give a systematic way for computing an algebraic 
differential equation from a linear differential equation with D-finite function coefficients.


This is a mathematical software article for the `Maple <https://www.maplesoft.com/>`__ package **NLDE** (NonLinear algebra and Differential Equations).
The package provides features to work with D-algebraic functions. These are functions that satisfy algebraic differential
equations (ADEs), i.e., differential equations that are polynomial in the independent variable and derivatives of the dependent variables.
The current main procedures are:

-  **unaryDalg**: for computing ADEs for rational expressions of a
   single D-algebraic function using elimination with Groebner bases.
-  **arithmeticDalg**: for computing ADEs for rational expressions of
   D-algebraic functions using elimination with Groebner bases.
-  **composeDalg**: for computing ADEs for compositions of D-algebraic
   functions partly using elimination with Groebner bases.
-  **diffDalg**: for computing ADEs for derivatives of D-algebraic
   functions using recursive elimination with by computing resultant.
-  **invDalg**: for computing ADEs for functional inverses of
   D-algebraic functions by explicit construction.
-  **AnsatzDalg**: a subpackage with main sub-procedures **unaryDeltak**
   and **arithmeticDeltak** for doing the same computation (with some
   extensions) as *unaryDalg* and *arithmeticDalg* by an algorithmic
   search based on linear algebra.
-  **DDfiniteToDalg**: for converting a holonomic ODE into an ADE having
   the same solutions.
-  **SysToMinDiffPoly**: for computing input-output equation of
   dynamical systems.
-  **OrderDegreeADE**: for computing the order and the degree of a given
   ADE. Often useful when the ADE displays on several lines.
-  **MultiDalg**: subpackage for operations with multivariate D-algebraic
   functions. The command, **arithmeticMDalg**, for arithmetic operations 
   is now available! (May 2023).
   
Installation
------------

The first step is to download the file :download:`NLDE.mla <NLDE.mla>`.

The easiest way to use **NLDE** in Maple is by putting the file NLDE.mla
in your working directory and include the lines

::

     > restart;

     > libname:=currentdir(), libname:

     > with(NLDE) 

at the beginning of your Maple worksheet (session). To avoid putting
these three lines in all worksheets, one can read the help page of the
:math:`\texttt{libname}` command.

Requirements and Dependencies
-----------------------------

The package can be used with any recent version of Maple (from 2019
onward). For Groebner bases computations the package relies on the
following Maple packages:

 - :math:`\texttt{Groebner}` 
 
 - :math:`\texttt{PolynomialIdeal}`

Licence
-------

-  licence: GNU General Public Licence v3.0.


Source
------

The source is hosted on Github at  `NLDE source <https://github.com/T3gu1a/D-algebraic-functions>`__.

Documentation and Examples
--------------------------

For documentation and examples check the following jupyter notebook: 

.. toctree::
	:maxdepth: 1
	:glob:

	NLDEdoc
	

Examples in a Maple Worksheet
-----------------------------

One can try the examples of the documentation with following Maple worksheet 
(click, and then save it with the extensino ".mw").

:download:`MapleWorksheet-NLDEdoc-examples.mw <MapleWorksheet-NLDEdoc-examples.mw>`.


References
----------

1. `D-algebraic functions <https://arxiv.org/abs/2301.02512>`__. Rida
   Ait El Manssour, Anna-Laura Sattelberger, Bertrand Teguia Tabuguia. January 2023.

2. `Operations for D-algebraic functions <https://arxiv.org/abs/2304.09675>`__ Bertrand Teguia Tabuguia. April 2023.

3. `Arithmetic of D-algebraic functions <https://arxiv.org/abs/2305.00702>`__. Bertrand Teguia Tabuguia. May 2023. Check this paper for the multivariate case.


Project page created: 19/04/2022

Project contributors: Bertrand Teguia Tabuguia

Corresponding author of this page: Bertrand Teguia Tabuguia, teguia@mis.mpg.de

Software used: Maple 2022. 

The implementation works on recent versions of Maple: 2018 to 2023.
Python versions for the Jupyter notebooks is Python 3.8.3.

System setup used: Processor: Intel(R) Core(TM) i5-10210U CPU @ 1.60GHz 2.11 GHz, 
Installed RAM: 16,0 GB (15,8 GB usable), System type: 64-bit operating system, x64-based processor,
Edition: Windows 11 Home Version 21H2.
