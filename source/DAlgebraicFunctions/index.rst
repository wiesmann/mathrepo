======================
D-Algebraic Functions
======================

| This page contains auxiliary files to the paper:
| Rida Ait El Manssour, Anna-Laura Sattelberger, and Bertrand Teguia Tabuguia: D-Algebraic Functions
| ARXIV: https://arxiv.org/abs/2301.02512 CODE: https://mathrepo.mis.mpg.de/DAlgebraicFunctions

ABSTRACT: Differentially-algebraic (D-algebraic) functions are solutions of 
polynomial equations in the function, its derivatives, and the independent variables. 
We revisit closure properties of these functions by providing constructive proofs. 
We present algorithms to compute algebraic differential equations for compositions 
and arithmetic manipulations of univariate D-algebraic functions and derive 
bounds for the order of the resulting differential equations. 
We apply our methods to examples in the sciences.


For instance, the exponential of the `Painlevé transcendent of type I <https://en.wikipedia.org/wiki/Painlev%C3%A9_transcendents>`_ is a D-algebraic function. 
It fullfils the following ADE (see Example 5.7 in our article):

.. math:: \begin{align*} 
				\hspace{-11mm} {24 x {w'(x)}^{2} w \! \left(x \right)^4+w \! \left(x \right)^{6}-2 w \! \left(x \right)^{5} w^{(3)}(x)+6 w''(x)  w'(x)  w \! \left(x \right)^4}+{w^{(3)}(x)}^{2} w \! \left(x \right)^4 \\ 
				\hspace{-11mm} -4 {w'(x)}^3-24 w''(x) {w'(x)}^{2} w \! \left(x \right)^3-6 w^{(3)}(x) w''(x)  w'(x)  w \! \left(x \right)^3+24 {w'(x)}^4 w \! \left(x \right)^{2}\\
				\hspace{-11mm} {+4 w^{(3)}(x) w'(x)^3 w \! \left(x \right)^{2}+9 {w''(x)}^{2} {w'(x)}^{2} w \! \left(x \right)^{2}}-12 w''(x) {w'(x)}^4 w \! \left(x \right)+4 {w'(x)}^{6}\,=\, 0 \,. \hspace{-11mm}
			\end{align*}

We have described two methods of computations, and each of them yields algorithms.
The algorithms of our second method are implemented in `Maple <https://www.maplesoft.com/>`_ 
in the package NLDE (NonLinear algebra and Differential Equations). The package can be downloaded
here :download:`NLDE.mla <NLDE.mla>`, and its up-to-date source is available at `NLDE source <https://github.com/T3gu1a/D-algebraic-functions>`__.
For the initial source, download the file :download:`sourceNLDE.mpl <sourceNLDE.mpl>`. 
The package is a user-friendly interface to the code for any Maple user who wants to try our implelentation.
Questions and comments are welcome. The use of the package, with example of our article, is illustrated in the following notebook:

.. toctree::
	:maxdepth: 1
	:glob:

	DAlgebraicFunc-Examples-Maple
	
The following file is a Maple worksheet with the same computations. 

:download:`DalgebraicFunc-Examples-Maple-Worksheet.mw <DAlgebraicFunc-Examples-Maple-Worksheet.mw>`.

The outputs are presented in the following printed pdf: 

:download:`Printed-PDF <DAlgebraicFunc-Examples-Maple-Worksheet.pdf>`.

We also have a `Macaulay2 <http://www2.macaulay2.com/Macaulay2/>`_ code that implements our first method. 
The following notebook demonstrates how it can be used:

.. toctree::
	:maxdepth: 1
	:glob:

	DAlgFunM2

Project page created: 14/11/2022

Project contributors: Rida Ait El Manssour, Anna-Laura Sattelberger, Bertrand Teguia Tabuguia

Corresponding author of this page: Bertrand Teguia Tabuguia, teguia@mis.mpg.de

Software used: Maple (2022),Macaulay2 (version 1.19.1).

The implementation works on recent versions of Maple: 2018 to 2023.

System setup used: Processor: Intel(R) Core(TM) i5-10210U CPU @ 1.60GHz 2.11 GHz, 
Installed RAM: 16,0 GB (15,8 GB usable), System type: 64-bit operating system, x64-based processor,
Edition: Windows 11 Home Version 21H2. WSL2 with Ubuntu 20.04.4 (to use Macaulay2 on Windows).
