=======================================
Generation of Matroids
=======================================


| This page presents an application of the paper:
| Mohamed Barakat, Reimer Behrends, Christoper Jefferson, Lukas Kühne, and Martin Leuner: On the generation of rank 3 simple matroids with an application to Terao' freeness conjecture
| In: SIAM journal on discrete mathematics, 35 (2021) 2, p. 1201-1223
| MIS-Preprint: `88/2020 <https://www.mis.mpg.de/publications/preprints/2020/prepr2020-88.html>`_ DOI: `10.1137/19M1296744 <https://dx.doi.org/10.1137/19M1296744>`_ ARXIV: https://arxiv.org/abs/1907.01073 CODE: https://mathrepo.mis.mpg.de/GenerationOfMatroids

The main contribution of this article is a large scale generation of a certain class of matroids in HPC-GAP. Subsequently, we stored these matroids in a publicly availabel Arango DB database: https://matroid.mathematik.uni-siegen.de.
This enabled us to answer the following question:


.. toctree::
	:maxdepth: 1
	:glob:

	MatroidGeneration

The code is presented as a Jupyter notebook based on the Julia package `CapAndHomalg <https://github.com/homalg-project/CapAndHomalg.jl>`_. It can be downloaded here: :download:`MatroidGeneration.ipynb`.

You can run the notebook online by clicking the binder badge below.

.. image:: ../binder_badge.svg
 :target: https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.mis.mpg.de%2Fkuhne%2Fbinderenvironments/GenerationOfMatroids?urlpath=git-pull%3Frepo%3Dhttps%253A%252F%252Fgitlab.mis.mpg.de%252Frok%252Fmathrepo%26urlpath%3Dtree%252Fmathrepo%252Fsource%252FGenerationOfMatroids%252FMatroidGeneration.ipynb%26branch%3Dmaster

Project page created: 19/10/2020

Code contributors: Mohamed Barakat, Lukas Kühne

Jupyter Notebook written by: Mohamed Barakat, 17/07/2020

Software used: Julia 1.5.2 and GAP 4.11.0

Project contributors: Mohamed Barakat, Lukas Kühne

Corresponding author of this page: Lukas Kühne, lukas.kuhne@mis.mpg.de

