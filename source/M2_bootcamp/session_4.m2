restart

ht = hashTable { 7 => "hello", b => "goodbye", QQ => "rational"}

ht.b
ht#b
ht#7
ht#QQ

b = 123
ht#b
ht.b


class QQ
class {1,2,3}

ancestors class {1,2,3}
ancestors class (1,2,3)

g = method()
g Thing := i -> i + 1

ancestors Ideal

g(7)
g(ideal 1)

-------------

-- trigger an error
error "error is here"

R = QQ[x,y]
ideal(x)
oo + ideal(y)
ooo + ideal(y^2)
oooo + ideal(y^3)

viewHelp "gbTrace"
gbTrace = 3

R = QQ[x,y,z, MonomialOrder => Lex]
I = ideal(x^9 + y^3 + z^2 - 1, x^7 + y^5 + z^3 - 1)
gens gb I -- Ctrl + C to interrups

R = QQ[x,y,z, MonomialOrder => Lex]
ideal(x^3 + y^3 + z^2 - 1, x^7 + y^5 + z^3 - 1)
gbTrace = 0
elapsedTime gens gb I;


oo
o84
o87

gens R
R_0
R_1

0_R
1_R

map(R^1,R^1,1)
methods map
viewHelp map

x = symbol x;
R = QQ[x_1,x_2]
x_1

f = n -> (
  x := symbol x;
  R := QQ[x_1..x_n];
  ideal gens R
)

x_1
f 3
x_1

restart
R = QQ[x,y]
x

f = () -> QQ[x,y]; -- bad, this "uses" the newly created ring,
                    -- messing up user variables

f = () -> QQ( monoid [x,y]); -- fix, doesn't use the new ring

f()

x
use R
x

