R = QQ[x,y,z]
I = ideal(x^2 + y^3 + z^2 - 1, x^7 + y^5 + z^3 -1)
gens gb I

S = newRing(R, MonomialOrder => Lex)
describe S
phi = map(S,R)
IS = phi I
gens gb IS

printWidth = 1000
-- alt+Z, ⌥ + Z
gens gb IS