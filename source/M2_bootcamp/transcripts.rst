========================
Code transcripts
========================

Session 1
----------------------------
:download:`session_1.m2 <session_1.m2>`

.. literalinclude:: session_1.m2
   :language: macaulay2
   :linenos:


Session 2
----------------------------
:download:`session_2.m2 <session_2.m2>`

.. literalinclude:: session_2.m2
   :language: macaulay2
   :linenos:


BestPackage
----------------------------
:download:`BestPackage.m2 <BestPackage.m2>`

.. literalinclude:: BestPackage.m2
   :language: macaulay2
   :linenos:


Session 4
----------------------------
:download:`session_4.m2 <session_4.m2>`

.. literalinclude:: session_4.m2
   :language: macaulay2
   :linenos:
