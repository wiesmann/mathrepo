# pkg> activate KhovanskiiSolving 
using KhovanskiiSolving 
using Oscar
using LinearAlgebra


############################### row 0 
k=3
m=6
n=k*(m-k)
r=0
A=vcat([ [3,5,6] for i=1:(9-2*r) ], [ [2,5,6] for i=1:r ] )
sum(codimSchubertVariety.(A,k,m))

K= QQ 
R,ϕ,vrs,M= plueckercoordinates(k,m,K);
Flags,F,leadexps,degs_F= equationsSchubertVariety(A,k,m,K);
length(F) 
dreg= sum(degs_F)-m+1 +1 
KM=@time get_KM(F,dreg,degs_F,ϕ,K,vrs,leadexps; reduce=false ) ;
Mul = @time get_commuting_matrices(F,dreg,degs_F,ϕ,K,vrs,leadexps);

K=GF(9716633) 
R,ϕ,vrs,M= plueckercoordinates(k,m,K);
Flags,F,leadexps,degs_F= equationsSchubertVariety(A,k,m,K);
length(F) 
dreg=5
HF_Gr(k,m,dreg) 
deg_Gr(k,m) 
KM=@time get_KM(F,dreg,degs_F,ϕ,K,vrs,leadexps) ;
Mul = @time get_commuting_matrices(F,dreg,degs_F,ϕ,K,vrs,leadexps);


######################## row 1 

k=3
m=6
n=k*(m-k)
r=1
A=vcat([ [3,5,6] for i=1:(9-2*r) ], [ [2,5,6] for i=1:r ] )
sum(codimSchubertVariety.(A,k,m))

K= QQ
(R,ϕ,vrs,M)= plueckercoordinates(k,m,K);
Flags,F,leadexps,degs_F= equationsSchubertVariety(A,k,m,K);
dreg=4 
HF_Gr(k,m,dreg) 
length(F) 
KM=@time get_KM(F,dreg,degs_F,ϕ,K,vrs,leadexps; reduce=false ) ;
Mul = @time get_commuting_matrices(F,dreg,degs_F,ϕ,K,vrs,leadexps); 



K=GF(9716633)
R,ϕ,vrs,M= plueckercoordinates(k,m,K);
Flags,F,leadexps,degs_F= equationsSchubertVariety(A,k,m,K);
dreg=4 
length(F) # 
KM=@time get_KM(F,dreg,degs_F,ϕ,K,vrs,leadexps; reduce=false ) ;
Mul = @time get_commuting_matrices(F,dreg,degs_F,ϕ,K,vrs,leadexps);



######################## row 2 
k=3
m=6
n=k*(m-k)
r=2
A=vcat([ [3,5,6] for i=1:(9-2*r) ], [ [2,5,6] for i=1:r ] )
sum(codimSchubertVariety.(A,k,m))

K= QQ
(R,ϕ,vrs,M)= plueckercoordinates(k,m,K);
Flags,F,leadexps,degs_F= equationsSchubertVariety(A,k,m,K);
dreg=3
HF_Gr(k,m,dreg) 
length(F) #13
KM=@time get_KM(F,dreg,degs_F,ϕ,K,vrs,leadexps; reduce=false ) ;
Mul = @time get_commuting_matrices(F,dreg,degs_F,ϕ,K,vrs,leadexps);


K=GF(9716633)
R,ϕ,vrs,M= plueckercoordinates(k,m,K);
Flags,F,leadexps,degs_F= equationsSchubertVariety(A,k,m,K);
dreg=3 
length(F) 
Mul = @time get_commuting_matrices(F,dreg,degs_F,ϕ,K,vrs,leadexps);
size(Mul[1])
KM=@time get_KM(F,dreg,degs_F,ϕ,K,vrs,leadexps; reduce=false ) ;
Mul,c= get_multiplication_matrices(KM,dreg,ϕ,K,vrs,leadexps);



################################ row 3 
k=3
m=6
n=k*(m-k)
r=3
A=vcat([ [3,5,6] for i=1:(9-2*r) ], [ [2,5,6] for i=1:r ] )
sum(codimSchubertVariety.(A,k,m))

K= QQ
(R,ϕ,vrs,M)= plueckercoordinates(k,m,K);
Flags,F,leadexps,degs_F= equationsSchubertVariety(A,k,m,K);
dreg=3
HF_Gr(k,m,dreg)
length(F) 
KM=@time get_KM(F,dreg,degs_F,ϕ,K,vrs,leadexps; reduce=false ) ;

Mul = @time get_commuting_matrices(F,dreg,degs_F,ϕ,K,vrs,leadexps);
size(Mul[1]) 


K=GF(9716633)
R,ϕ,vrs,M= plueckercoordinates(k,m,K);
Flags,F,leadexps,degs_F= equationsSchubertVariety(A,k,m,K);
dreg=3  
length(F) 
Mul = @time get_commuting_matrices(F,dreg,degs_F,ϕ,K,vrs,leadexps);
size(Mul[1]) 

KM=@time get_KM(F,dreg,degs_F,ϕ,K,vrs,leadexps; reduce=false ) ;
Mul,c= get_multiplication_matrices(KM,dreg,ϕ,K,vrs,leadexps);





############################### row 4 
k=3
m=6
n=k*(m-k)
r=4
A=vcat([ [3,5,6] for i=1:(9-2*r) ], [ [2,5,6] for i=1:r ] )
sum(codimSchubertVariety.(A,k,m))

K= QQ
R,ϕ,vrs,M= plueckercoordinates(k,m,K);
Flags,F,leadexps,degs_F= equationsSchubertVariety(A,k,m,K);
length(F) 
dreg=2 
HF_Gr(k,m,dreg) 
KM=@time get_KM(F,dreg,degs_F,ϕ,K,vrs,leadexps; reduce=false ) ;

Mul = @time get_commuting_matrices(F,dreg,degs_F,ϕ,K,vrs,leadexps);
size(Mul[1])


K=GF(9716633)
R,ϕ,vrs,M= plueckercoordinates(k,m,K);
Flags,F,leadexps,degs_F= equationsSchubertVariety(A,k,m,K);
length(F) 
dreg=2 
KM=@time get_KM(F,dreg,degs_F,ϕ,K,vrs,leadexps; reduce=false ) ;


Mul = @time get_commuting_matrices(F,dreg,degs_F,ϕ,K,vrs,leadexps);
size(Mul[1]) 
