############
Terms of Use
############

This website is built on the `MPI MiS GitLab <https://gitlab.mis.mpg.de/rok/mathrepo/>`_. Currently,
contributors are foremostly members of the `Nonlinear Algebra group <https://www.mis.mpg.de/nlalg/nlalg-people.html>`_
and their collaborators. `Access <https://gitlab.mis.mpg.de/users/sign_in?redirect_to_referer=yes>`_ can be obtained by
informally asking for a local account. 

The main criteria for material to be fit for MathRepo is that it is research data and that it is
mathematics. Information on how to prepare new contributions for publication on MathRepo can be found in the `Readme
<https://gitlab.mis.mpg.de/rok/mathrepo/-/blob/develop/README.md>`_ on the GitLab referenced above. That document and
the `Template <https://gitlab.mis.mpg.de/rok/mathrepo/-/blob/develop/template.rst>`_ contain the current standards for
research-data presentation and metadata supply. 

Ideally, contributions are published together with a corresponding preprint article on `arXiv <https://arxiv.org>`_, and these two sources reference each other. For citation of MathRepo
content, use the direct link to an individual subpage. URLs will be stable for the forseeable future. For contributions which are larger than 50 MB in size, please contact the
MathRepo maintainers before creating a page. For larger data sets https://keeper.mpdl.mpg.de from the Max-Planck Digital Library is an alternative, the registration is instant if you use your MPI email address.
 So far, for computations the programming languages that are used are listed `here <https://mathrepo.mis.mpg.de/software.html>`_. The use of open source software is encouraged. 

We recommend to use an open, permissive license for MathRepo pages. However, it is possible to choose a different license, which we ask you to clearly indicate on your respective subpage. You find a list of licences here: https://spdx.org/licenses/ 

The article "The mathematical research-data repository MathRepo" written by Claudia Fevola and Christiane Görgen and
published in the Computer Algebra Rundbrief, No. 70, 2022, provides more background on this repository and outlines its
past and future development. A `preprint version <https://arxiv.org/abs/2202.04022>`_ is available on arXiv. 

This page was created by Christiane Görgen in June 2022 and last updated by Tabea Bacher in July 2023.
