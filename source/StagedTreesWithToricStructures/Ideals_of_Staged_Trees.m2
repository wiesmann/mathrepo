
--Creates the probability (source) ring  
probRing= method()
probRing(List) :=  (leaves) -> (
   R := QQ[for i in leaves list p_i];
   R
   )


-- Creates the list of indices of parameters
parameterIndices = method()
parameterIndices(List) := (leaves) -> (
T :={};
l :=local l;
i := local i;
for l in leaves do(
K := for  i in (0..#l-1) list (drop(l,-i));
T =unique join(T,K));
T
)

-- Creates the parameter (target) ring
parameterRing = method()
parameterRing(List) := (leaves) -> (
    T := parameterIndices(leaves);
    S :=tensor(QQ[z],QQ[for i in T list s_i]);
S
)


-- Collects sets of parameters that must sum to one 
sumto1 = method()
sumto1(List) := (leaves) -> (
    T := parameterIndices(leaves);
psum1 :={};
l := local l;
t := local t;
for t in T do(
    A := {};
    for l in T do(
	if t=!=l and drop(t,-1)==drop(l,-1) then  A=append(A,l));
	if A=!={} then  psum1=unique append(psum1,append(A,t)));
psum1
)



-- Creates the sum to one conditions 
sumto1Eq = method()
sumto1Eq(List) := (leaves) -> (
        S := parameterRing(leaves);
  ps := sumto1(leaves);
  p := local p;
  i := local i;
  I := ideal(substitute(0,S));
for p in ps do(
   f := for i in p list s_i;
   I = I +ideal(substitute(1-(f//sum),S)));
I
)

-- Creates the staged conditions
stagedEq = method();
stagedEq(List) := (stages) -> (
    S := parameterRing(leaves);
  t := local t;
  i := local i;
  j := local j;
  k := local k;
  I := ideal(substitute(0,S));
 for t in stages do(
     for i in drop(t,1) do(
	 for j in drop(t,1) do(
	 for k from 1 to t_0 do(
	I = I+ideal(substitute(s_(append(i,k)),S) - substitute(s_(append(j,k)),S));
	))));
I
)

--target quotient ring taking in consideration sums to one and stages conditions
quotientRing= method()
quotientRing(List,List) :=  (leaves,stages) -> (
  S := parameterRing(leaves);
  I := ideal(substitute(mingens sumto1Eq(leaves),S));
  J := ideal(substitute(mingens stagedEq(stages),S));
quotS := S/(I+J);
quotS
)


--creates the staged tree vanishing ideal
stagedIdeal= method()
stagedIdeal(List,List,Ring) := (leaves,stages,R) -> (
      S := parameterRing(leaves);
     quotS := quotientRing(leaves,stages);
      l := local l;
      imMap := {};
      for l in leaves do(
	  use S;
K := (for  i in (0..#l-1) list  s_(drop(l,-i)));
imMap =append (imMap, substitute(product(K),quotS)));
imMap =(substitute(z,quotS))*(toList imMap);     
      psi =map(quotS,R,imMap);
  ker(psi)
      )


TEST 
--EXAMPLE 1 (coincides with Example 7.2 in the paper) 
--Input 
leaves={{1,1,1},{1,1,2},{1,2},{2,1},{2,2}}
stages={{2,{},{1}},{2,{2},{1,1}}}
--output 
R=probRing(leaves) --source ring where ideal lives
I=stagedIdeal(leaves,stages,R) --staged tree vanishing ideal

--other methods 
parameterIndices(leaves)
S=parameterRing(leaves)
psum1=sumto1(leaves)
sumto1Eq(leaves)
stagedEq(stages)
quotientRing(leaves,stages)

--linear transformation 
S=QQ[p_1..p_(length(leaves))];
M=map(R,S, 
    {p_{1,1,1}+p_{1,1,2},
    p_{1,1,2},
    p_{1,1,1}+p_{1,1,2}+2*p_{1,2},
    p_{1,1,1}+p_{1,1,2}+4*p_{2,1}+4*p_{2,2},
    p_{1,1,2}+4*p_{2,2}} 
)
Minv=inverse M
Iinv=trim Minv(I) 
