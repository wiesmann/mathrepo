# An implementation of Algorithm 1 in "Gibbs Manifolds" by Dmitrii Pavlov, Bernd Sturmfels and Simon Telen
# Julia v1.8.3
# November 22, 2022

using Oscar
using LinearAlgebra
using IterTools

d = 5 # dimension of the LSSM
n = 4 # size of the matrices

# define the names of the variables
y_strings = ["y$i" for i = 1:d]
λ_strings = ["λ$j" for j = 1:n]
z_strings = ["z$j" for j = 1:n]
x_strings = ["x$k$l" for l = 1:n for k = 1:l]

# R is our big polynomial ring
R, vrs = PolynomialRing(QQ,vcat(y_strings,λ_strings,z_strings,x_strings,["w"]))

# Rename the output variables "vrs" for convenience
y = vrs[1:d]
λ = vrs[d+1:d+n]
z = vrs[d+n+1:d+2*n]
x = vrs[d+2*n+1:end-1]
X = [ i<=j ? x[Int(j*(j-1)/2+i)] : x[Int(i*(i-1)/2+j)] for i=1:n, j=1:n ]
w = vrs[end]

# Definition of the LSSM
# A = [sum(y) y[1] y[2]; y[1] sum(y) y[3]; y[2] y[3] sum(y)] # Introductory example
# A = [0 0  y[1];   0 y[1] y[2]; y[1] y[2] 0] # Segre symbol [3], simplified version of Example 2.3
# A = [4*y[1] y[1] y[1]; y[1] 3*y[1] y[1]; y[1] y[1] 3*y[1]] # Example 2.8 
# A = [0 0 y[1] 0; 0 y[1] y[2] 0; y[1] y[2] 0 0; 0 0 0 y[1]+y[2]] # Example 4.1 part 1
# A = [0 0 y[1] 0; 0 y[1] y[2] 0; y[1] y[2] 0 0; 0 0 0 y[1]] # Example 4.1 part 2
A = [y[1]+y[3] y[5] y[4] 0;y[5] y[1] 0 y[4]; y[4] 0 y[2]+y[3] y[5];0 y[4] y[5] y[2]] # Example 6.3, QOT 


# Step 1
id = diagm(ones(Int,n))
S = MatrixSpace(R,n,n)
P = det(S(A-w.*id))
c = coefficients(P,length(vrs))
σ = [sum([prod(λ[I]) for I in IterTools.subsets([i for i = 1:n],d)]) for d = n:-1:1]

# Step 2
E1 = [(-1)^(i) * c[i] + σ[i] for i = 1:n]
E1 = (minimal_primes(ideal(E1)))[1]

# Steps 3-4
K = FractionField(R)
ϕ = sum([prod([K(z[i]).//K(λ[i]-λ[j]).*(K.(A-λ[j].*id)) for j in setdiff(1:n,i)]) for i = 1:n])
LCD = prod([λ[s[1]]-λ[s[2]] for s in IterTools.subsets([i for i = 1:n],2)])
E2 = [(LCD.*(ϕ - X))[i,j] for i = 1:n for j = 1:i]
E2 = numerator.(E2)

# Steps 5-8
E3 = []
lam_deps = groebner_basis(eliminate(E1,y))
qlin_deps=[]
for i=1:length(lam_deps)
    if total_degree(lam_deps[i]) == 1
        push!(qlin_deps,lam_deps[i])
    end
end
exponent_vecs_poly = [[coeff(qlin_deps[j],λ,id[i,:]) for i=1:n] for j=1:length(qlin_deps)]
exponent_vecs = [[parse(Int,string(exponent_vecs_poly[i][j])) for j=1:n] for i=1:length(exponent_vecs_poly)]
for i=1:length(exponent_vecs)
    pos_part =[exponent_vecs[i][j] >= 0 ? exponent_vecs[i][j] : 0 for j=1:n]
    neg_part =[exponent_vecs[i][j] <0 ? -exponent_vecs[i][j] : 0 for j=1:n]
    bino = prod(z[j]^pos_part[j] for j=1:n) - prod(z[j]^neg_part[j] for j=1:n)
    push!(E3,bino)
end

# Steps 9-10
E1 = gens(E1)
E2
E3
II = ideal([E1;E2;E3;1-w*LCD])
# Step 11
J = eliminate(II,[y;λ;z;w])