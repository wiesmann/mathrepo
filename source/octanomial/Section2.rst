====================================
Section 2: Algebra and Combinatorics
====================================

The eight coefficients of the octanomial model 

.. math::
   a \cdot xyz + b \cdot xyw + c \cdot xzw + d \cdot yzw + e \cdot x^2 y + f \cdot x y^2 + g \cdot z^2 w + h \cdot z w^2

   
are quintics in the moduli parameters :math:`d_1, d_2,d_3,d_4, d_5, d_6`. They were found by a calculation in Macaulay2 carried out with the help of Mike Stillman and described in the proof of Proposition 2.1.

.. math:: \begin{matrix}
          a   \,= &  d_1  d_3  d_2  d_4 ( d_1{+} d_3{-} d_2{-} d_4)+ d_2  d_4  d_5  d_6 ( d_2{+} d_4{-} d_5{-} d_6)+ 
	  	  \qquad \qquad \qquad \qquad \qquad \qquad \qquad \qquad \quad \\
	  & d_5  d_6  d_1  d_3 ( d_5{+}d_6{-}d_1{-}d_3) + d_5  d_6 ( d_5{+} d_6) ( d_1^2{+}d_3^2{-}d_2^2{-}d_4^2)+
	  	  \qquad \qquad \qquad \qquad \qquad \qquad \qquad \quad\\
	  & d_1  d_3 ( d_1{+} d_3) ( d_2^2{+} d_4^2{-} d_5^2{-} d_6^2) + d_2  d_4 ( d_2{+} d_4) ( d_5^2{+} d_6^2{-} d_1^2{-} d_3^2)
	  	  \qquad \qquad \qquad \qquad \qquad \qquad \qquad  \\
	  b  \,= &  d_1  d_2  d_3  d_5 ( d_1{+} d_2{-} d_3{-} d_5)+ d_3  d_5  d_4  d_6 ( d_3{+} d_5{-} d_4{-} d_6)+
	  	  \qquad \qquad \qquad \qquad \qquad \qquad \qquad \qquad \quad  \\
	  & d_4  d_6  d_1  d_2 ( d_4{+} d_6{-} d_1{-} d_2) + d_4  d_6 ( d_4{+} d_6) ( d_1^2{+} d_2^2{-} d_3^2{-} d_5^2)+
	  	  \qquad \qquad \qquad \qquad \qquad \qquad \qquad  \quad  \\
	  & d_1  d_2 ( d_1{+} d_2)( d_3^2{+} d_5^2{-} d_4^2{-} d_6^2)+ d_3  d_5 ( d_3{+} d_5) (d_4^2{+} d_6^2{-} d_1^2{-} d_2^2)
	  	  \qquad \qquad \qquad \qquad \qquad \qquad \qquad  \\
	  c  \, = &  d_1  d_3  d_2  d_5 ( d_1{+} d_3{-} d_2{-} d_5)+ d_2  d_5  d_4  d_6 ( d_2{+} d_5{-} d_4{-} d_6)+
	  	  \qquad \qquad \qquad \qquad \qquad \qquad \qquad \qquad \quad  \\
	  & d_4  d_6  d_1  d_3 ( d_4{+} d_6{-} d_1{-} d_3) +  d_4  d_6 ( d_4{+} d_6) ( d_1^2{+} d_3^2{-} d_2^2{-} d_5^2)+
	  	  \qquad \qquad \qquad \qquad \qquad \qquad \qquad \quad  \\
	  & d_1  d_3 ( d_1{+} d_3) ( d_2^2{+} d_5^2 {-} d_4^2{-} d_6^2)+ d_2  d_5 ( d_2{+} d_5) ( d_4^2{+} d_6^2{-} d_1^2{-} d_3^2)
	  	  \qquad \qquad \qquad \qquad \qquad \qquad \qquad  \\
	  d  \,= &  d_1  d_2  d_3  d_4 ( d_1{+} d_2{-} d_3{-} d_4)+ d_3  d_4  d_5  d_6 ( d_3{+} d_4{-} d_5{-} d_6)+
	  	  \qquad \qquad \qquad \qquad \qquad \qquad \qquad \qquad \quad  \\
	  & d_5  d_6  d_1  d_2 ( d_5{+} d_6{-} d_1{-} d_2) +  d_5  d_6 ( d_5{+} d_6) ( d_1^2{+} d_2^2{-} d_3^2{-} d_4^2)+
	  	  \qquad \qquad \qquad \qquad \qquad \qquad \qquad  \quad \\
	  & d_1  d_2 ( d_1{+} d_2) ( d_3^2{+} d_4^2{-} d_5^2{-} d_6^2) + d_3  d_4 ( d_3{+} d_4) ( d_5^2{+} d_6^2{-} d_1^2{-} d_2^2)
	  	  \qquad \qquad \qquad \qquad \qquad \qquad \qquad  \\
	  e \,= & -( d_1+ d_3+ d_5) ( d_2+ d_4+ d_6) ( d_1- d_5) ( d_2- d_6) ( d_3- d_4) 
	  \qquad \qquad \qquad \qquad \qquad \qquad \qquad \qquad  \\
	  f \,= & -( d_1+ d_2+ d_4) ( d_3+ d_5+ d_6) ( d_1- d_4) ( d_2- d_5) ( d_3- d_6) 
	  \qquad \qquad \qquad \qquad \qquad \qquad \qquad \qquad  \\
	  g \, = & -( d_1+ d_3+ d_4) ( d_2+ d_5+ d_6) ( d_1- d_4) ( d_2- d_6) ( d_3- d_5)
	  \qquad \qquad \qquad \qquad \qquad \qquad \qquad \qquad  \\
	  h \, = & -( d_1+ d_2+ d_5) ( d_3+ d_4+ d_6) ( d_1- d_5) ( d_3- d_6) ( d_2- d_4)
	  \qquad \qquad \qquad \qquad \qquad \qquad \qquad \qquad 
	  \end{matrix} 


The file  :download:`all_lines <all_lines.txt>` contains the formulas for all :math:`27` lines and the coordinates of their :math:`135` intersection points in term of the moduli parameters. See the file :download:`all_pointspols <all_pointspols.txt>` for the :math:`135` intersection points with polynomial coordinates.   

The file :download:`matrix <matrix.txt>` contains a :math:`3\times 3` matrix whose entries are linear forms in :math:`x,y,z,w` and coefficients in  :math:`d_1, d_2,d_3,d_4, d_5, d_6`. Its determinat gives the determinantal representation of our octanomial model. 

The statement in Remark 2.2 can be verified using the following :download:`maple code <remark2.2>`. 

The support of the octanomial surface is

.. math:: \mathcal{A} \, = \, \bigl\{ (1110), (1101), (1011), (0111), (2100), (1200), (0021), (0012) \bigr\}.

The list of the :math:`53` unimodular triangulations of this point configuration can be found :download:`here <unimodular_trgs.txt>`.
  
