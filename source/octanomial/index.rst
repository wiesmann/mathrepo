======================================
An Octanomial Model for Cubic Surfaces
======================================

| This page contains the supplementary material for the article:
| Marta Panizzut, Emre Can Sertöz, and Bernd Sturmfels: An octanomial model for cubic surfaces
| In: Le Matematiche, 75 (2020) 2, p. 517-536
| DOI: `10.4418/2020.75.2.8 <https://dx.doi.org/10.4418/2020.75.2.8>`_ ARXIV: https://arxiv.org/abs/1908.06106 CODE: https://mathrepo.mis.mpg.de/octanomial

Cubic surfaces are represented by a polynomial with eight terms

.. math::
   a \cdot xyz + b \cdot xyw + c \cdot xzw + d \cdot yzw + e \cdot x^2 y + f \cdot x y^2 + g \cdot z^2 w + h \cdot z w^2,

with coefficients written in moduli from the :math:`\textrm{E}_6` hyperplane arrangement. The codes and outputs of the  symbolic and :math:`p`-adic numerical computations are presented following the structure of the paper.


.. toctree::
   :maxdepth: 2

   Section2.rst
   Section3.rst
   Section4.rst

The magma codes used to run the computations in Section 4 of the paper can be found in the github repository https://github.com/emresertoz/pAdicCubicSurface.


Project contributors: Marta Panizzut, Emre Can Sertöz, and Bernd Sturmfels

Software used: Magma