=================================================================================
Crossing the transcendental divide: from translation surfaces to algebraic curves
=================================================================================

| This page contains auxiliary files to the paper:
| Türkü Özlüm  Çelik,  Samantha  Fairchild,  Yelena Mandelshtam
| Crossing the transcendental divide: from translation surfaces to algebraic curves
| ARXIV: `https://arxiv.org/abs/2211.00304 <https://arxiv.org/abs/2211.00304>`_
| CODE: `https://mathrepo.mis.mpg.de/Tsurfaces2Acurves/ <https://mathrepo.mis.mpg.de/Tsurfaces2Acurves/index.html>`_

**Abstract of paper.**
We study constructing an algebraic curve from a Riemann surface given via a translation surface,
which is a collection of finitely many polygons in the plane with sides identified by translation. 
We use the theory of discrete Riemann surfaces to give an algorithm for approximating the Jacobian
variety of a translation surface whose polygon can be decomposed into squares. We first implement the algorithm in the case of :math:`L`
shaped polygons where the algebraic curve is already known.  The algorithm is also implemented in any genus for specific examples of Jenkins--Strebel representatives, a family of translation surfaces that, until now, lived squarely on the analytic side of the transcendental 
divide between Riemann surfaces and algebraic curves. 
Using Riemann theta functions, we give numerical experiments and resulting conjectures up to genus :math:`5`.


**Approximating Riemann matrices** We provide an algorithm which inputs a translation surface as a polygon and a level of approximation :math:`n`
and outputs the discrete Riemann matrix. We implement the algorithm in two cases. Note the second implementation for Jenkins--Strebel representatives is an improvement over what was used for the :math:`L` shape.

* The :math:`L` shape:
	* In :download:`main.m<LCode/main.m>` the user inputs a side length :math:`\lambda`, and a level :math:`n`. The output is the associated :math:`2\times 2` Riemann matrix.
	* The called functions include :download:`matrixdata.m<LCode/matrixdata.m>` to build the matrix, as well as 3 smaller functions for subdividing (:download:`subdivide.m<LCode/subdivide.m>`), holomorphicitiy conditions (:download:`colindshol.m<LCode/colindshol.m>`), and periodicity conditions (:download:`colindsper.m<LCode/colindsper.m>`).
* Jenkins--Strebel reperesentatives:
	* In :download:`js.m <JSCode/js.m>` the user inputs a translation surface by inputting the genus :math:`g` and the level :math:`n`. The output is the associated :math:`g\times g` Riemann matrix.
	* The two functions called are :download:`matrix.m <JSCode/matrix.m>` to construct the sparse matrix from the linear relations given in the algorithm, and :download:`varindex.m <JSCode/varindex.m>` which organizes the different variables used.
	* In the paper, Tables 5, 6, and 7 have rounded output, but for convenience we include the complete output for level :math:`n=7` in genus :download:`3<JSg345n7/g3n7.txt>`, :download:`4<JSg345n7/g4n7.txt>`, :download:`5<JSg345n7/g5n7.txt>`. 

* Jenkins--Strebel representatives for genus 2 and changing side lengths.
	* In :download:`js2.m <JS2Code/js2.m>` we fix genus 2, and allow two side lengths to vary with parameters :math:`\lambda` and :math:`\mu` where we require :math:`\lambda` and :math:`\mu` are rational numbers with odd numerators and odd denominators. The output is the associated :math:`2\times 2` Riemann matrix.
	* The two functions called are :download:`matrix2.m <JS2Code/matrix2.m>` and :download:`varindex2.m <JS2Code/varindex2.m>` with the same analgous functions as above.


**Using Theta functions to interpret the Riemann matrix approximations**
	
	* The following code was used in Section 3.3.3, in SageMath we have :download:`genus 2<genus2.txt>`, :download:`genus 3<genus3.txt>`, and :download:`genus 4<genus4.txt>`, and in Magma :download:`genus 5<genus5.txt>`.

| Project page created: 28/10/2022
| Project contributors: Türkü Özlüm Çelik, Samantha Fairchild, Yelena Mandelshtam


| Software used: Magma (V2.26-8), Matlab (R2022b 9.13.0.2049777, 64-bit glnxa64), SageMath (9.2)
| System used: Linux x3850-1 5.10.0-12-amd64 #1 SMP Debian 5.10.103-1 (2022-03-07) x86_64 GNU/Linux

| Corresponding author of this page: Samantha Fairchild, samantha.fairchild@mis.mpg.de
| Last updated 28/03/23.



