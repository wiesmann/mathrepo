function index = varindex(g, n, str,i, j)
%VARINDEX gives the index of the variable. 
% For the x_(i, j), str = "NA" and we just use i, j
%Use "A or B" if the variable is one of the periods. then i = the index of
%the period and j = 1 if white, 0 if black
% and use "sum" for the sum of the Bi's. then the i = which Bis we are summing
% and j doesn't matter
if str == "NA"
    index = (3^n+1)*i + j + 1;
elseif str == "A"
    index = ((4*g-4)*3^n+1)*(3^n+1) + 2*i +j -1;
elseif str == "B"
    index = ((4*g-4)*3^n+1)*(3^n+1) + 2*g + 2*i +j -1;
elseif str == "sum"
    index = ((4*g-4)*3^n+1)*(3^n+1) + 4*g + i;
end

