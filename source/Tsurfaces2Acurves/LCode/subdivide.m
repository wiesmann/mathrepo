%this function creates a dictionary (indices) which stores variable names
%and a corresponding index. This is the index of the column which
%represents the variable in the equations matrix
function indices = subdivide(lam, n)
    [N, D] = rat(lam);
    step = 2*D; %size of boxes is 1/ LCM(lam.denominator, 2)
    %this is the map that takes input "i, j" and outputs its index/variable
    %number. For example 'indices("0, 0")' outputs '0' and the coefficients of x_0_0
    %will appear in the 0th column of the matrix
    indices = containers.Map('keyType', 'char', 'valueType', 'double');
    count = 1;
    formatSpec = "%d, %d";
    for j = 0:step * 3^n
        for i = 0:2*N * 3^n
            key = sprintf(formatSpec, i, j);
            indices(key) = count;
            count = count +1;
        end
    end
    for j = step * 3^n + 1 : 2*N * 3^n
        for i = 0 : step * 3^n
            key = sprintf(formatSpec, i, j);
            indices(key) = count;
            count = count +1;
        end
    end
    indices("A1b") = count;
    indices("A1w") = count + 1;
    indices("B1b") = count + 2;
    indices("B1w") = count + 3;
    indices("A2b") = count + 4;
    indices("A2w") = count + 5;
    indices("B2b") = count + 6;
    indices("B2w") = count + 7;
    indices("sum1") = count + 8;
    indices("sum2") = count + 9;
    count = count+9;
end