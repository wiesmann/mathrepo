===========================
Meet and greet with OSCAR
===========================
Some tutorials presented at "OSCAR rings in the new year" by Taylor Brysiewicz, Anna-Laura Sattelberger, Mima Stanojkovski, and Rosa Winter on January 14, 2021 at the MPI MiS. Check out the Jupyter notebooks to get started with `OSCAR <https://oscar.computeralgebra.de/>`_! A documentation (under construction) for OSCAR.jl can be found `here <https://github.com/oscar-system/Oscar.jl>`_.
 
.. toctree::
	:titlesonly:
	:maxdepth: 1

	OscarGroups
	OscarNumberTheory
	OscarPolytopes
 	OscarRingsIdeals

Project page created: 21/01/2021

Software used: Julia, Oscar, Polymake, Singular

Language versions: Julia 1.5.3, Oscar.jl 0.5.0, Polymake: 4.2, Polymake.jl 0.5.3, Singular.jl 0.4.2, Singular 2.3.1-4

Project contributors: Taylor Brysiewicz (OscarPolytopes), Anna-Laura Sattelberger (OscarRingsIdeals), Mima Stanojkovski (OscarGroups), and Rosa Winter (OscarNumberTheory)

Corresponding author of this page: Anna-Laura Sattelberger, anna-laura.sattelberger@mis.mpg.de
