##################
Chapter 9: Tensors
##################

This chapter was presented by Paul Goerlach and Madeline Brandt. The code is written by Tim Seynnaeve in Macaulay2.

Exercise 3: Consider the :math:`3 \times 3 \times 2 \times 2` tensor defined by the multilinear form :math:`x_1y_1z_1w_1+x_2y_2z_2w_2`. Determine all quadruples of singular vectors of :math:`T`.

.. code-block:: macaulay2

                restart
                K=QQ
                R=K[x1,x2,x3,y1,y2,y3,z1,z2,w1,w2]
                T=x1*y1*z1*w1+x2*y2*z2*w2

                --We compute the gradient of T
                grad = jacobian ideal T

A point :math:`([x_1:x_2:x_3],[y_1:y_2:y_3],[z_1:z_2],[w_1:w_2])` in :math:`\mathbb{P}^2 \times \mathbb{P}^2 \times \mathbb{P}^1 \times \mathbb{P}^1` is a singular tuples if
it is fixed by the gradient map. To check if 2 points are equal in :math:`\mathbb{P}^{n-1}`, we view them as rows of a :math:`2 \times n` matrix and ask for the rank to be 1.

.. code-block:: macaulay2

                M=(vars R)||(transpose grad)
                I=minors(2,submatrix(M,{0,1},{0,1,2}))+minors(2,submatrix(M,{0,1},{3,4,5}))
                     +minors(2,submatrix(M,{0,1},{6,7}))+minors(2,submatrix(M,{0,1},{8,9}))

The ideal I cuts out a subvariety of :math:`\mathbb{P}^2 \times \mathbb{P}^2 \times \mathbb{P}^1 \times \mathbb{P}^1` consisting of all singular tuples.
We try to decompose it into its irreducible components:

.. code-block:: macaulay2

                decompose I

This decomposition contains several obsolete components. For example the ideal (x1,x2,x3):
there are no points in  :math:`\mathbb{P}^2 \times \mathbb{P}^2 \times \mathbb{P}^1 \times \mathbb{P}^1` with :math:`x_1=x_2=x_3=0`. This is because the ideal :math:`(x_1,x_2,x_3)`
contains the "irrelevant ideal" of  :math:`\mathbb{P}^2 \times \mathbb{P}^2 \times \mathbb{P}^1 \times \mathbb{P}^1`.
We can get rid of these obsolete components by saturating with respect to the irrelevant ideal:

.. code-block:: macaulay2

                I=saturate(I,ideal(x1,x2,x3))
                I=saturate(I,ideal(y1,y2,y3))
                I=saturate(I,ideal(z1,z2))
                I=saturate(I,ideal(w1,w2))

Now we can analyze the components of our variety of singular tuples:

.. code-block:: macaulay2

                D=decompose I

We can look at their (co)dimensions and degrees:

.. code-block:: macaulay2

                apply(D,i->(codim i,degree i))

So there are 19 2-dimensional planes of singular tuples, plus 18 additional singular tuples,
10 of which are real and 8 of which are complex
