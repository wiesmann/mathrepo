{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "using QuinticSpectrahedra"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [],
   "source": [
    "using HomotopyContinuation, LinearAlgebra"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Computing the combinatorial types of a quintic spectrahedra.\n",
    "\n",
    "Given a linear space of $5 \\times 5$ real symmetric matrices of the form $$A(x)=x_1I+x_2A_2+x_3A_3+x_4A_4.$$\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The corresponding $\\textit{quintic spectrahedron}$ is the collection \n",
    "$$\\{x \\in \\mathbb{RP}^3 \\mid A(x) \\text{ is semidefinite}\\}.$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The algebraic boundary of this spectrahedron is the variety $$V(\\text{det}(A(x))) \\subset \\mathbb{CP}^3$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "called a $\\textit{quintic symmetroid}$."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Quintic symmetroids generically have $20$ singularities, some of which are real and some of which are nonreal. The real singularities may be partitioned into those which are on the spectrahedron, and those which are not. The $\\textit{combinatorial type}$ of a spectrahedron is $(\\rho,\\sigma)$ where $\\rho$ counts the number of real singularities on the symmetroid, and $\\sigma$ counts the real singularities on the spectrahedron."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Example: computing the singularities on 100 random quintic symmetroids"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "\r",
      "\u001b[32mSolving for 100 parameters... 100%|█████████████████████| Time: 0:00:06\u001b[39m\r\n",
      "\u001b[34m  # parameters solved:  100\u001b[39m\r\n",
      "\u001b[34m  # paths tracked:      2000\u001b[39m\n"
     ]
    }
   ],
   "source": [
    "@var x[1:4], a[1:3,1:5,1:5]\n",
    "A=vcat([LinearAlgebra.I],[Symmetric(a[i,:,:]) for i in 1:3])\n",
    "Ax=sum([x[i]*A[i] for i in 1:4])\n",
    "eqs=[differentiate(det(Ax),x[i]) for i in 1:4]\n",
    "affine_chart=sum(x.*randn(Float64,4))+randn()\n",
    "eqs=vcat(eqs,affine_chart)\n",
    "symm_parameters = unique(vcat(vec(A[2]),vec(A[3]),vec(A[4])))\n",
    "quintic_system=System(eqs;parameters=symm_parameters)\n",
    "init_params=randn(ComplexF64,45)\n",
    "init_sols=solve(quintic_system,target_parameters=init_params)\n",
    "samples=solve(quintic_system,\n",
    "              solutions(init_sols),\n",
    "              start_parameters=init_params,\n",
    "              target_parameters=[randn(Float64,45) for i in 1:100]);"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "$\\texttt{samples}$ is now a list of $100$ pairs: \n",
    "\n",
    "$\\bullet$ the second element in the pair is a list of the $15 \\times 3 =45$ entries of the matrices $A_2,A_3,A_4$ \n",
    "\n",
    "$\\bullet$ the first element in the pair is a list of the $20$ nodes on $V(\\text{det}(A(x)))$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "\n",
    "### Computing the combinatorial type of a quintic spectrahedron"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "\n",
    "We can verify the combinatorial type of one such pair using the following function. \n",
    "\n",
    "$\\textbf{Disclaimer}$: the computation of the combinatorial type below is numerical. Thus, it is subject to error.\n",
    "\n",
    "(Next, we will introduce a function which certifies the combinatorial type)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "(6, 2)"
      ]
     },
     "execution_count": 4,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "S=samples[1]\n",
    "S_sols=S[1]\n",
    "S_params=S[2]\n",
    "C=combinatorial_type(S)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Certifying the combinatorial type of a quintic spectrahedron\n",
    "The function Certify_Combinatorial_Type takes the entries of $A_2,A_3,A_4$ as input. \n",
    "\n",
    "It computes a square subsystem of the polynomial system above, solves for the $64=20+44$ solutions, and then provably determines which $20$ are solutions to the full system. For each of these $20$ nodes, it also computes the values of its principal minors and then certifies their signs, thus determining the combinatorial type."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "System solved\n",
      "Certification performed\n",
      "Every sign was certifiable\n",
      "Certified 20 nodal solutions\n",
      "Certified 6 real solutions\n",
      "Certified 2 semidefinite solutions (0 PSD & 2 NSD)\n",
      "\n",
      "Certification was successful\n"
     ]
    }
   ],
   "source": [
    "Certify_Combinatorial_Type(S_params);"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Certifying each type of quintic spectrahedra\n",
    "The dictionary, Witness, encodes witnesses of each type of quintic spectrahedron, indexed by their combinatorial type. \n",
    "\n",
    "Below, we certify that there exists a quintic spectrahedron with $20$ nodes on its Euclidean boundary."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [],
   "source": [
    "W=Witness[20,20];"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "System solved\n",
      "Certification performed\n",
      "Every sign was certifiable\n",
      "Certified 20 nodal solutions\n",
      "Certified 20 real solutions\n",
      "Certified 20 semidefinite solutions (0 PSD & 20 NSD)\n",
      "\n",
      "Certification was successful\n"
     ]
    }
   ],
   "source": [
    "Certify_Combinatorial_Type(W);"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### The following command will produce the defining polynomial for a spectrahedron of the given combinatorial type. You can copy the string into surf to produce a visualization of the symmetroid. Note the affine chart chosen to produce this polynomial is $\\text{tr}(A(x))=5$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "\"1 - 1.11022302462516e-16*x + 1.11022302462516e-16*y + 13.5340331245054*x*y - 7.50082190675219*x*y^2 - 30.5947158533286*x*y^3 + 16.3556137746578*x*y^4 + 10.1250625635179*x*z - 13.5238469359924*x*z^2 - 34.7107588504292*x*z^3 + 46.2797918544673*x*z^4 - 11.8011128061791*x^2*y + 106.520994774996*x^2*y^2 - 36.1346631378685*x^2*y^3 - 0.912883555677102*x^2*z + 93.1119661438995*x^2*z^2 - 40.7493198322911*x^2*z^3 - 112.40944986166*x^3*y - 2.57851657765584*x^3*y^2 - 50.2183265153595*x^3*z + 34.7187817032043*x^3*z^2 + 87.3087978248684*x^4*y - 18.3629244608769*x^4*z - 4.88506621973035*y*z - 0.0839712657765846*y*z^2 + 24.6476526523819*y*z^3 - 17.589041046132*y*z^4 + 1.87109089280124*y^2*z + 51.980046097328*y^2*z^2 - 45.2540998481445*y^2*z^3 + 19.0583489603145*y^3*z - 34.1059841873924*y^3*z^2 - 13.6372255329615*y^4*z - 4.81380504464746*x*y*z - 103.884695693223*x*y*z^2 + 101.846614227372*x*y*z^3 - 86.5420161963045*x*y^2*z + 143.840185198655*x*y^2*z^2 + 88.8512079931257*x*y^3*z + 111.442737741989*x^2*y*z - 117.912907068058*x^2*y*z^2 - 153.635803466823*x^2*y^2*z + 109.985405146789*x^3*y*z - 18.7032651432321*x^2 + 13.3748754238749*x^3 + 61.4555517635348*x^4 - 46.351832498193*x^5 - 7.04515792259221*y^2 + 5.31535175043697*y^3 + 4.0935232861909*y^4 - 2.46438263066026*y^5 - 10.5285778845096*z^2 + 4.40736695602141*z^3 + 19.4756254479112*z^4 - 7.58970631225188*z^5\""
      ]
     },
     "execution_count": 8,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "str=jsurf((10,10))"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Julia 1.5.2",
   "language": "julia",
   "name": "julia-1.5"
  },
  "language_info": {
   "file_extension": ".jl",
   "mimetype": "application/julia",
   "name": "julia",
   "version": "1.5.2"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
