==========================================
Hirota Varieties and Rational Nodal Curves
==========================================

| This page contains auxiliary files to the paper:
| Claudia Fevola and Yelena Mandelshtam: Hirota Varieties and Rational Nodal Curves
| ARXIV: https://arxiv.org/abs/2203.00203 CODE: https://mathrepo.mis.mpg.de/HirotaVarietyRationalNodalCurve

The Hirota variety parameterizes solutions to the KP equation arising from a degenerate Riemann theta function. In this work we study in detail the Hirota variety arising from a rational nodal curve of genus :math:`g`. Of particular interest is the irreducible subvariety defined as the image of the parameterization map

.. math:: \begin{align*}
    		\phi \;:\; \mathbb{C}^{3g} \quad & \dashrightarrow (\mathbb{C}^*)^{2^g}\times \mathbb{W}\mathbb{P}^{3g-1}\\
    		(\lambda_1,\dots,\lambda_g,\kappa_1,\kappa_2,\dots,\kappa_{2g}) & \longrightarrow (a_{{\bf c}_1},a_{{\bf c}_2},\dots,a_{{\bf c}_{2^g}},{\bf u}, {\bf v}, {\bf w})
	  \end{align*}

where the coordinates :math:`{\bf a} = (a_{{\bf c}_1}, a_{{\bf c}_2},\dots,a_{{\bf c}_{2^g}})` are indexed by the points in :math:`\mathcal{C}=\{0,1\}^g`. The image of :math:`\phi` is defined as follows


.. math::  \begin{matrix} 
    		u_i = \kappa_{2i-1}-\kappa_{2i},\quad v_i = \kappa_{2i-1}^2- \kappa_{2i}^2, \quad w_i = \kappa_{2i-1}^3-\kappa_{2i}^3, \qquad \text{for all } i=1,\dots, g,\\
 		a_{\mathbf{c}} = \displaystyle\prod_{\substack{i,j\in I\\i<j}} (\kappa_i - \kappa_j) \displaystyle\prod_{i: c_i = 1} \lambda_i \, \quad \text{ where } I = \{2i: c_i = 0\} \cup \{2i-1: c_i = 1\}, \quad \text{for all } \mathbf{c}\in \mathcal{C}. 
 \end{matrix}


We call the closure of the image of :math:`\phi` the :math:`\textbf{main component}` of the Hirota variety and denote it by :math:`\mathcal{H}_{\mathcal{C}}^M`. Proving that this is an irreducible component of the Hirota variety corresponds to solving a :math:`\textbf{weak Schottky problem}` for rational nodal curves. We solve this problem up to genus 9 using computational tools. The proof of this is mainly computational. Our code, implemented in :math:`\verb|Macaulay2|`, shows that the Jacobian matrix of the Hirota variety :math:`\mathcal{H}_{\mathcal{C}}` evaluated ad a general point in the image of the parametrization has the desired rank. This shows that the parametrization map is dominant into the main component :math:`\mathcal{H}_{\mathcal{C}}^M`.  

.. toctree::
   :maxdepth: 2

   MainComponent.rst
   MainComponent2.rst

We include the code computing the image of the Abel map of an irreducible rational nodal curve. This computation justifies the choice of the parameterization for the parameters :math:`a_i` in the map :math:`\phi` above. This is also implemented in :math:`\verb|Macaulay2|`.

.. toctree::
   :maxdepth: 2

   ThetaDivisor.rst


.. role:: raw-html(raw)
    :format: html

Project page created: 28/02/2022 :raw-html:`<br />`
Project contributors: Claudia Fevola and Yelena Mandelshtam :raw-html:`<br />`

Software used: Macaulay2 (Version 1.13) :raw-html:`<br />`
System setup used: MacBook Pro with macOS Monterey 12.0.1, Processor 2,7 GHz Intel Core i7, Memory 16 GB 2133 MHz LPDDR3, Graphics Intel Iris Graphics 550 1536 MB. :raw-html:`<br />`

Corresponding author of this page: Claudia Fevola, claudia.fevola@mis.mpg.de