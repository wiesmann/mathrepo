########
Examples
########

All examples are for :math:`K=\mathbb Q` equipped with the 2-adic valuation.


Random lexicographical Groebner bases of ideals in shape position
-----------------------------------------------------------------

A random lexicographical Groebner basis :math:`G` of an ideal :math:`I\subseteq \mathbb Q[x_1,\ldots,x_n]` in shape position is of the form

.. math::

   \{ f_n(x_n), x_{n-1} - f_{n-1}(x_n), \ldots, x_1-f_1(x_n) \},

where

* :math:`f_n,f_{n-1},\ldots,f_1` are univariate polynomials of degrees :math:`d,d-1,\ldots,d-1` respectively,
* all coefficients are of the form :math:`2^\lambda \cdot (2k+1)` for some random valuation :math:`0\leq\lambda < 100` and some random :math:`0\leq k < 5000`.

In our paper, we have constructed :download:`100 random lexicographical Groebner bases<timingExamplesShape.sing>` for :math:`d=2,4,8,12,16,20,24`. You can load all examples into Singular by running the script as follows:

.. code-block:: none

                       SINGULAR                                /  Development
  A Computer Algebra System for Polynomial Computations       /   version 4.1.1
                                                           0<
  by: W. Decker, G.-M. Greuel, G. Pfister, H. Schoenemann     \   Feb 2018
  FB Mathematik der Universitaet, D-67653 Kaiserslautern       \
  > <"lexicographicalGroebnerBases.sing".

Differentiated by degree, the timings are:

.. image:: timingShape.sing





27 distinct tropical lines on a random honeycomb cubic
------------------------------------------------------

In [PV19], it is shown that a general tropical cubic surface :math:`\text{Trop}(f)` in :math:`\mathbb T\mathbb P^3` in honeycomb form contains 27 distinct tropical lines, which must then be tropicalizations of the 27 lines on any algebraic cubic :math:`\text{V}(f)` in :math:`\mathbb P^3`.

In our paper, we have constructed :download:`1000 random cubic polynomials<timingExamplesCubics.sing>`, whose coefficients are pure powers of 2, and computed the lines on the tropical cubic surfaces as points in :math:`\text{Grass}(2,4)` in the affine chart where Pluecker coordinate :math:`p_{34}` is 1.

You can load all examples into Singular by running the script as follows:

.. code-block:: none

                       SINGULAR                                /  Development
  A Computer Algebra System for Polynomial Computations       /   version 4.1.1
                                                           0<
  by: W. Decker, G.-M. Greuel, G. Pfister, H. Schoenemann     \   Feb 2018
  FB Mathematik der Universitaet, D-67653 Kaiserslautern       \
  > <"timingExamplesCubics.sing".

Differentiated by splitting field, the timings are:

.. image:: timingCubics.sing

References
----------
[PV19] M. Panizzut and M. D. Vigeland. Tropical Lines on Cubic Surfaces. eprint:
arXiv:`0708.3847v2 <https://arxiv.org/abs/0708.3847>`_. 2019
