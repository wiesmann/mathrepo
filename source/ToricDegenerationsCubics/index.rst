=====================================
Toric Degenerations of Cubic Surfaces
=====================================

| This website contains auxiliary material to the article:
| Maria Donten-Bury, Paul Görlach, and Milena Wrobel: Towards classifying toric degenerations of cubic surfaces
| In: Le Matematiche, 75 (2020) 2, p. 537-557
| MIS-Preprint: `90/2019 <https://www.mis.mpg.de/publications/preprints/2019/prepr2019-90.html>`_ DOI: `10.4418/2020.75.2.9 <https://dx.doi.org/10.4418/2020.75.2.9>`_ ARXIV: https://arxiv.org/abs/1909.06690 CODE: https://mathrepo.mis.mpg.de/ToricDegenerationsCubics

Theorem 4.1 of this article describes a fan structure on the tropical variety TGr(3,6) adapted to the classification of Khovanskii bases. The following Singular code generates this fan structure and extracts information about moneric classes. The main results of the computation are stated in Section 4 of the above article. Exhaustive output is provided below.

* :download:`Singular code for subdivision of TGr(3,6) <subdivideTGr36.sing>`
* :download:`Auxiliary file 1 <equivalentBinomials.sing>`
* :download:`Auxiliary file 2 <S6a.sing>`
* :download:`Auxiliary file 3 <S6b.sing>`


Output:

* :download:`Maximal cones of the subdivision up to symmetry <subdivisionModuloSymmetry.txt>`
* :download:`Moneric classes up to symmetry <monericClasses.txt>`


38 of these combinatorial types lead to toric degenerations. The following Macaulay2 code computes them by filtering out which moneric classes from the above output form a Khovanskii basis. We output the choices of initial monomials and the corresponding toric ideals.

* :download:`Macaulay2 code for filtering out Khovanskii bases <filterOutKhovanskii.m2>`
* :download:`Auxiliary file <monericClassesM2.txt>`

Output:

* :download:`Khovanskii bases up to symmetry <khovanskiiClasses.txt>`
* :download:`Toric ideals of the degenerations <toricDegenerations.txt>`

Last update: December 9, 2019

Project contributors: Maria Donten-Bury, Paul Görlach, and Milena Wrobel

Software used: Macaulay2, Singular