=============================================
The Geometries of Jordan nets and Jordan webs
=============================================

| This page contains auxiliary files to the paper:
| Arthur Bik and Henrik Eisenmann: The Geometries of Jordan nets and Jordan webs
| In: Annali di matematica pura ed applicata, 201 (2022) 5, p. 2413-2464 
| MIS-Preprint: `1/2022 <https://www.mis.mpg.de/publications/preprints/2022/prepr2022-1.html>`_ DOI: `10.4418/2021.76.2.3 <https://doi.org/10.1007/s10231-022-01204-y>`_ ARXIV: https://arxiv.org/abs/2201.04403 CODE: https://mathrepo.mis.mpg.de/GeometriesJordanNetsWebs

We compute numerically whether degenerations between orbits of different Jordan nets and Jordan nets in :math:`\mathcal{S}^n` exist for :math:`n=4,5`. The computations have been verified on all known degenerations and all known cases where no degeneration exists.

We also verify the degenerations found in the appendix.

.. toctree::
   :maxdepth: 1
   :glob:
   
   Degenerations_between_Jordan_Spaces

The source code can be downloaded here: 

:download:`Degenerations_between_Jordan_Spaces.ipynb <Degenerations_between_Jordan_Spaces.ipynb>`

Project page created: 09/04/2022

Project contributors: Arthur Bik, Henrik Eisenmann
 
Code written by: Henrik Eisenmann

Software used: Julia (Version 1.5.2)

Corresponding author of this page: Henrik Eisenmann, henrik.eisenmann@mis.mpg.de
