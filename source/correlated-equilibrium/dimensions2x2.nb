(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 13.0' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[     13268,        361]
NotebookOptionsPosition[     12563,        340]
NotebookOutlinePosition[     12958,        356]
CellTagsIndexPosition[     12915,        353]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{

Cell[CellGroupData[{
Cell[BoxData[
 RowBox[{
  RowBox[{"(*", " ", 
   RowBox[{
   "This", " ", "file", " ", "contains", " ", "code", " ", "for", " ", 
    "computing", " ", "the", " ", "region", " ", "of", " ", "full", " ", 
    "dimensionality", " ", "for", " ", "the", " ", "Correlated", " ", 
    "Equilibria", " ", "polytopes", " ", "of", " ", "2", "x2", " ", "games", 
    " ", "which", " ", "is", " ", "used", " ", "in", " ", "Example", " ", 
    "4.3", " ", "of", " ", "our", " ", 
    RowBox[{"paper", ".", " ", "\[IndentingNewLine]", "This"}], " ", "idea", 
    " ", "in", " ", "general", " ", "is", " ", "discussed", " ", "in", " ", 
    "Section", " ", "4", " ", "of", " ", "our", " ", 
    RowBox[{"paper", "."}]}], " ", "*)"}], "\[IndentingNewLine]", 
  "\[IndentingNewLine]", 
  RowBox[{"(*", " ", 
   RowBox[{
   "This", " ", "creates", " ", "a", " ", "list", " ", "of", " ", 
    "indeterminates", " ", "with", " ", "appropriate", " ", "upper", " ", 
    "and", " ", "lower", " ", "subscripts"}], "*)"}], "\[IndentingNewLine]", 
  RowBox[{
   RowBox[{
    RowBox[{"y", " ", "=", " ", 
     RowBox[{"{", 
      RowBox[{
       RowBox[{"{", 
        RowBox[{
         RowBox[{"Subsuperscript", "[", 
          RowBox[{"Y", ",", "1", ",", "1"}], "]"}], ",", " ", 
         RowBox[{"Subsuperscript", "[", 
          RowBox[{"Y", ",", "2", ",", "1"}], "]"}]}], "}"}], ",", 
       RowBox[{"{", 
        RowBox[{
         RowBox[{"Subsuperscript", "[", 
          RowBox[{"Y", ",", "1", ",", "2"}], "]"}], ",", " ", 
         RowBox[{"Subsuperscript", "[", 
          RowBox[{"Y", ",", "2", ",", "2"}], "]"}]}], "}"}]}], "}"}]}], ";"}],
    "\[IndentingNewLine]", "\[IndentingNewLine]", 
   RowBox[{"(*", " ", 
    RowBox[{"This", " ", "is", " ", "the", " ", "matrix", " ", "A", 
     RowBox[{"(", "Y", ")"}], " ", "which", " ", "is", " ", "described", " ", 
     "in", " ", "Section", " ", "4.", " ", "This", " ", "is", " ", "simply", 
     " ", "the", " ", "matrix", " ", "whose", " ", "rows", " ", "are", " ", 
     "the", " ", "inequalities", " ", "which", " ", "cut", " ", "out", " ", 
     "the", " ", "correlated", " ", "equilibria", " ", "cone", " ", "of", " ",
      "a", " ", "2", "x2", " ", "game", " ", "for", " ", "any", " ", "given", 
     " ", 
     RowBox[{"Y", "."}]}], " ", "*)"}], "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"A", " ", "=", 
     RowBox[{"{", 
      RowBox[{
       RowBox[{"{", 
        RowBox[{
         RowBox[{
          RowBox[{"y", "[", 
           RowBox[{"[", 
            RowBox[{"1", ",", "1"}], "]"}], "]"}], "[", 
          RowBox[{"1", ",", "2"}], "]"}], ",", " ", 
         RowBox[{
          RowBox[{"y", "[", 
           RowBox[{"[", 
            RowBox[{"1", ",", "2"}], "]"}], "]"}], "[", 
          RowBox[{"1", ",", "2"}], "]"}], ",", " ", "0", ",", " ", "0"}], 
        "}"}], ",", "\n", "\t ", 
       RowBox[{"{", 
        RowBox[{"0", ",", " ", "0", ",", 
         RowBox[{"-", 
          RowBox[{
           RowBox[{"y", "[", 
            RowBox[{"[", 
             RowBox[{"1", ",", "1"}], "]"}], "]"}], "[", 
           RowBox[{"1", ",", "2"}], "]"}]}], ",", " ", 
         RowBox[{"-", 
          RowBox[{
           RowBox[{"y", "[", 
            RowBox[{"[", 
             RowBox[{"1", ",", "2"}], "]"}], "]"}], "[", 
           RowBox[{"1", ",", "2"}], "]"}]}]}], "}"}], ",", "\n", "\t ", 
       RowBox[{"{", 
        RowBox[{
         RowBox[{
          RowBox[{"y", "[", 
           RowBox[{"[", 
            RowBox[{"2", ",", "1"}], "]"}], "]"}], "[", 
          RowBox[{"1", ",", "2"}], "]"}], ",", " ", "0", ",", " ", 
         RowBox[{
          RowBox[{"y", "[", 
           RowBox[{"[", 
            RowBox[{"2", ",", "2"}], "]"}], "]"}], "[", 
          RowBox[{"1", ",", "2"}], "]"}], ",", " ", "0"}], "}"}], ",", "\n", 
       "          ", 
       RowBox[{"{", 
        RowBox[{"0", ",", " ", 
         RowBox[{"-", 
          RowBox[{
           RowBox[{"y", "[", 
            RowBox[{"[", 
             RowBox[{"2", ",", "1"}], "]"}], "]"}], "[", 
           RowBox[{"1", ",", "2"}], "]"}]}], ",", " ", "0", ",", "  ", 
         RowBox[{"-", 
          RowBox[{
           RowBox[{"y", "[", 
            RowBox[{"[", 
             RowBox[{"2", ",", "2"}], "]"}], "]"}], "[", 
           RowBox[{"1", ",", "2"}], "]"}]}]}], "}"}], ",", "\n", "\t", 
       RowBox[{"{", 
        RowBox[{"1", ",", " ", "0", ",", " ", "0", ",", " ", "0"}], "}"}], 
       ",", "\n", "         ", 
       RowBox[{"{", 
        RowBox[{"0", ",", " ", "1", ",", " ", "0", ",", " ", "0"}], "}"}], 
       ",", "\n", "         ", 
       RowBox[{"{", 
        RowBox[{"0", ",", " ", "0", ",", " ", "1", ",", " ", "0"}], "}"}], 
       ",", "\n", "         ", 
       RowBox[{"{", 
        RowBox[{"0", ",", " ", "0", ",", " ", "0", ",", " ", "1"}], "}"}]}], 
      "}"}]}], ";"}], "\[IndentingNewLine]", 
   RowBox[{"A", "//", "MatrixForm"}]}]}]], "Input",
 CellChangeTimes->{{3.869558056903778*^9, 3.869558142472169*^9}, {
  3.869558272739193*^9, 3.8695583934122677`*^9}, {3.8695585651588793`*^9, 
  3.8695586589431334`*^9}, {3.8695587170796137`*^9, 3.8695587337052813`*^9}, {
  3.8695587638145723`*^9, 3.869559066109187*^9}, {3.869559104179254*^9, 
  3.86955916182911*^9}, {3.869634381150777*^9, 3.869634385259625*^9}, {
  3.871097305497262*^9, 
  3.871097322508829*^9}},ExpressionUUID->"be3d62dd-6e20-4f58-b6c8-\
419014004ae8"],

Cell[BoxData[
 TagBox[
  RowBox[{"(", "\[NoBreak]", GridBox[{
     {
      RowBox[{
       TemplateBox[{"Y", "1", "1"},
        "Subsuperscript"], "[", 
       RowBox[{"1", ",", "2"}], "]"}], 
      RowBox[{
       TemplateBox[{"Y", "2", "1"},
        "Subsuperscript"], "[", 
       RowBox[{"1", ",", "2"}], "]"}], "0", "0"},
     {"0", "0", 
      RowBox[{"-", 
       RowBox[{
        TemplateBox[{"Y", "1", "1"},
         "Subsuperscript"], "[", 
        RowBox[{"1", ",", "2"}], "]"}]}], 
      RowBox[{"-", 
       RowBox[{
        TemplateBox[{"Y", "2", "1"},
         "Subsuperscript"], "[", 
        RowBox[{"1", ",", "2"}], "]"}]}]},
     {
      RowBox[{
       TemplateBox[{"Y", "1", "2"},
        "Subsuperscript"], "[", 
       RowBox[{"1", ",", "2"}], "]"}], "0", 
      RowBox[{
       TemplateBox[{"Y", "2", "2"},
        "Subsuperscript"], "[", 
       RowBox[{"1", ",", "2"}], "]"}], "0"},
     {"0", 
      RowBox[{"-", 
       RowBox[{
        TemplateBox[{"Y", "1", "2"},
         "Subsuperscript"], "[", 
        RowBox[{"1", ",", "2"}], "]"}]}], "0", 
      RowBox[{"-", 
       RowBox[{
        TemplateBox[{"Y", "2", "2"},
         "Subsuperscript"], "[", 
        RowBox[{"1", ",", "2"}], "]"}]}]},
     {"1", "0", "0", "0"},
     {"0", "1", "0", "0"},
     {"0", "0", "1", "0"},
     {"0", "0", "0", "1"}
    },
    GridBoxAlignment->{"Columns" -> {{Center}}, "Rows" -> {{Baseline}}},
    GridBoxSpacings->{"Columns" -> {
        Offset[0.27999999999999997`], {
         Offset[0.7]}, 
        Offset[0.27999999999999997`]}, "Rows" -> {
        Offset[0.2], {
         Offset[0.4]}, 
        Offset[0.2]}}], "\[NoBreak]", ")"}],
  Function[BoxForm`e$, 
   MatrixForm[BoxForm`e$]]]], "Output",
 CellChangeTimes->{
  3.8695581165391073`*^9, 3.869558322656259*^9, 3.869558365803647*^9, 
   3.869558614316433*^9, {3.869558650169445*^9, 3.869558674629548*^9}, {
   3.869558857093129*^9, 3.869558926768105*^9}, 3.8695591413074617`*^9, {
   3.8696343888160877`*^9, 3.869634396889114*^9}},
 CellLabel->
  "Out[31]//MatrixForm=",ExpressionUUID->"c15c49c5-5bcb-4ebe-9a82-\
7f1b6887b003"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{"(*", " ", 
   RowBox[{"The", " ", "cone", " ", "C", 
    RowBox[{"(", "Y", ")"}], " ", "is", " ", "full", " ", "dimensional", " ", 
    "if", " ", "and", " ", "only", " ", "if", " ", "there", " ", "is", " ", 
    "a", " ", "point", " ", "in", " ", "its", " ", "relative", " ", 
    "interior", " ", "meaning", " ", "there", " ", "exists", " ", "p", " ", 
    "such", " ", "that", " ", "A", 
    RowBox[{"(", "y", ")"}], "p", " ", "0.", " ", "The", " ", "following", 
    " ", "code", " ", "performs", " ", "quantifier", " ", "elimination", " ", 
    "to", " ", "determine", " ", "which", " ", 
    RowBox[{"Y", "'"}], "s", " ", "guarantee", " ", "this", " ", 
    "condition"}], " ", "*)"}], "\[IndentingNewLine]", 
  RowBox[{
   RowBox[{
    RowBox[{"p", " ", "=", " ", 
     RowBox[{"{", 
      RowBox[{"p11", ",", " ", "p12", ",", " ", "p21", ",", " ", "p22"}], 
      "}"}]}], ";"}], "\[IndentingNewLine]", "\[IndentingNewLine]", 
   RowBox[{"(*", " ", 
    RowBox[{
     RowBox[{
     "This", " ", "makes", " ", "the", " ", "list", " ", "of", " ", 
      "inequalities", " ", "Ap"}], " ", ">", " ", "0"}], " ", "*)"}], 
   "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"ineqs", "=", " ", 
     RowBox[{"Table", "[", 
      RowBox[{
       RowBox[{
        RowBox[{
         RowBox[{"(", 
          RowBox[{"A", ".", "p"}], ")"}], "[", 
         RowBox[{"[", "i", "]"}], "]"}], ">", " ", "0"}], ",", " ", 
       RowBox[{"{", 
        RowBox[{"i", ",", "1", ",", " ", "8"}], "}"}]}], "]"}]}], ";"}], 
   "\[IndentingNewLine]", "\[IndentingNewLine]", 
   RowBox[{"(*", " ", 
    RowBox[{
    "This", " ", "eliminates", " ", "the", " ", "euqnaitfier", " ", "and", 
     " ", "computes", " ", "the", " ", "inequalities", " ", "in", " ", "the", 
     " ", "space", " ", "of", " ", 
     RowBox[{"Y", "'"}], 
     RowBox[{"s", "."}]}], "*)"}], "\[IndentingNewLine]", 
   RowBox[{"(*", " ", 
    RowBox[{
     RowBox[{"As", " ", "expected"}], ",", " ", 
     RowBox[{
     "the", " ", "resulting", " ", "semialgebraic", " ", "set", " ", 
      "consists", " ", "of", " ", "two", " ", "orthants"}]}], " ", "*)"}], 
   "\[IndentingNewLine]", 
   RowBox[{"Reduce", "[", 
    RowBox[{
     RowBox[{"Exists", "[", 
      RowBox[{
       RowBox[{"{", 
        RowBox[{"p11", ",", " ", "p12", ",", " ", "p21", ",", " ", "p22"}], 
        "}"}], ",", " ", "ineqs"}], "]"}], ",", " ", "Reals"}], 
    "]"}]}]}]], "Input",
 CellChangeTimes->{{3.86955914994576*^9, 3.869559385115654*^9}, {
  3.869559468615641*^9, 3.869559564834177*^9}, {3.8695596199061213`*^9, 
  3.869559742845929*^9}, {3.869559787641952*^9, 3.869559904456203*^9}, {
  3.86955995019709*^9, 3.8695599504097757`*^9}},
 CellLabel->"In[26]:=",ExpressionUUID->"dafb5a9a-9f66-46b6-95ae-226f2f797ce3"],

Cell[BoxData[
 RowBox[{
  RowBox[{"(", 
   RowBox[{
    RowBox[{
     RowBox[{
      TemplateBox[{"Y", "2", "2"},
       "Subsuperscript"], "[", 
      RowBox[{"1", ",", "2"}], "]"}], "<", "0"}], "&&", 
    RowBox[{
     RowBox[{
      TemplateBox[{"Y", "2", "1"},
       "Subsuperscript"], "[", 
      RowBox[{"1", ",", "2"}], "]"}], "<", "0"}], "&&", 
    RowBox[{
     RowBox[{
      TemplateBox[{"Y", "1", "2"},
       "Subsuperscript"], "[", 
      RowBox[{"1", ",", "2"}], "]"}], ">", "0"}], "&&", 
    RowBox[{
     RowBox[{
      TemplateBox[{"Y", "1", "1"},
       "Subsuperscript"], "[", 
      RowBox[{"1", ",", "2"}], "]"}], ">", "0"}]}], ")"}], "||", 
  RowBox[{"(", 
   RowBox[{
    RowBox[{
     RowBox[{
      TemplateBox[{"Y", "2", "2"},
       "Subsuperscript"], "[", 
      RowBox[{"1", ",", "2"}], "]"}], ">", "0"}], "&&", 
    RowBox[{
     RowBox[{
      TemplateBox[{"Y", "2", "1"},
       "Subsuperscript"], "[", 
      RowBox[{"1", ",", "2"}], "]"}], ">", "0"}], "&&", 
    RowBox[{
     RowBox[{
      TemplateBox[{"Y", "1", "2"},
       "Subsuperscript"], "[", 
      RowBox[{"1", ",", "2"}], "]"}], "<", "0"}], "&&", 
    RowBox[{
     RowBox[{
      TemplateBox[{"Y", "1", "1"},
       "Subsuperscript"], "[", 
      RowBox[{"1", ",", "2"}], "]"}], "<", "0"}]}], ")"}]}]], "Output",
 CellChangeTimes->{
  3.869559402333355*^9, 3.869559479481485*^9, 3.869559639536435*^9, 
   3.8695597433539343`*^9, {3.869559813472625*^9, 3.869559858221142*^9}, 
   3.869559905162077*^9, 3.869559953278287*^9, {3.869634391812398*^9, 
   3.8696343949648542`*^9}},
 CellLabel->"Out[28]=",ExpressionUUID->"677a7dc1-5097-4598-9bd5-41789a67ebb2"]
}, Open  ]]
},
WindowSize->{1680, 908},
WindowMargins->{{0, Automatic}, {Automatic, 0}},
FrontEndVersion->"13.0 for Mac OS X x86 (64-bit) (February 4, 2022)",
StyleDefinitions->"Default.nb",
ExpressionUUID->"3c971f5d-7906-4f03-afc8-03f22b1856c3"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[CellGroupData[{
Cell[580, 22, 5379, 126, 346, "Input",ExpressionUUID->"be3d62dd-6e20-4f58-b6c8-419014004ae8"],
Cell[5962, 150, 2105, 65, 178, "Output",ExpressionUUID->"c15c49c5-5bcb-4ebe-9a82-7f1b6887b003"]
}, Open  ]],
Cell[CellGroupData[{
Cell[8104, 220, 2787, 64, 220, "Input",ExpressionUUID->"dafb5a9a-9f66-46b6-95ae-226f2f797ce3"],
Cell[10894, 286, 1653, 51, 37, "Output",ExpressionUUID->"677a7dc1-5097-4598-9bd5-41789a67ebb2"]
}, Open  ]]
}
]
*)

