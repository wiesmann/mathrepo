-- This file contains code for computing the algebraic boundary of the oriented matroid strata for 2x3 games which is used in Example 5.5 of our paper
-- This idea and the relationship between combinatorial types and the oriented matroid strata is discussed in Section 5 of our paper

-- This creates the ring of y variables which correspond to the coefficients on the inequalities of the correlated equilibria polytope
R = QQ[y_{{1, 1}, {1, 2}}, y_{{1, 2},{1, 2}}, y_{{2, 1},{1, 2}}, y_{{2, 2},{1, 2}}]

-- This is the matrix who's rows give the inequalities of the Correlated Equilibria cone for a 2x3 game
A = {{y_{{1, 1}, {1, 2}}, y_{{1, 2},{1, 2}}, 0, 0},
   	 {0, 0, -y_{{1, 1},{1, 2}}, -y_{{1, 2},{1, 2}}},
   	 {y_{{2, 1},{1, 2}}, 0, y_{{2, 2},{1, 2}}, 0},
     {0, -y_{{2, 1},{1, 2}}, 0,  -y_{{2, 2},{1, 2}}},
   	 {1, 0, 0, 0},
     {0, 1, 0, 0},
     {0, 0, 1, 0},
     {0, 0, 0, 1}};

A = matrix A;

-- This creates the list of maximal minors. The signs of these minors encode the oriented matroid of A(Y)
maxMinors = delete(0_R, subsets(toList(0..7), 4) / (s -> det(A^s)))

-- Since a subset s is a basis if and only if the det(A_s) != 0, the algebraic boundary of the oriented matroid strata is given by the union of the varieties of the above minors
-- These minors need not be irreducible though. This means we may be able to describe the boundary with significantly fewer polynomials.
-- To do this we first compute the primary decomposition of the variety of each minors
minorIdeals = maxMinors / (f -> ideal(f))

-- We now compute the minimal primes of each ideal and remove the duplicates
-- The resulting list gives us the irreducible polynomials which define the algebraic boundary of the oriented matroid strata
irreducibles = minorIdeals / (f -> minimalPrimes f) // flatten // unique
