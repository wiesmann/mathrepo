======================================
Combinatorics of Correlated Equilibria
======================================

| This page contains auxiliary files to the article:
| Marie-Charlotte Brandenburg, Benjamin Hollering, and Irem Portakal: *Combinatorics of Correlated Equilibria*.
| ARXIV: https://arxiv.org/abs/2209.13938 
| CODE: https://mathrepo.mis.mpg.de/correlated-equilibrium


.. image:: traffic-lights.png
   :width: 65%

.. image:: face_lattice.png
   :width: 30%



**Abstract:** We study the correlated equilibrium polytope :math:`P_G` of a game :math:`G` from a combinatorial viewpoint. We introduce the region of full-dimensionality for this class of polytopes, and prove that it is a semialgebraic set for any game. Through the use of the oriented matroid strata, we propose a structured method for describing combinatorial types of :math:`P_G`, and show that for :math:`(2 \times n)`-games, the algebraic boundary of each stratum is the union of coordinate hyperplanes and binomial hypersurfaces. We provide a computational proof that for :math:`(2 \times 3)`-games there exists a unique combinatorial type of maximal dimension.

|

3d Model
--------

The set of correlated equilibria of a fixed game is a polytope, and the Nash equilibria of the game are the intersection of this polytope with the Segre variety inside the probability simplex. Below is a 3d model of the correlated equilibrium polytope (blue) inside the probability simplex (yellow) the Segre variety (orange), and the Nash equilibria (green). Click here to see the 3d model in full screen: `[Full Screen] <../_static/correlated-equilibrium-3d/correlated-equilibrium-polytope.html>`_  


.. raw:: html
    
    <p align="center"><iframe src="../_static/correlated-equilibrium-3d/correlated-equilibrium-polytope.html"  width="55%" height = "400px" frameBorder="0"></iframe></p>


|

Supplementary Material
----------------------

This page contains supplementary material for the computations performed in the article. All files with ending ``.nb`` are run with :math:`\verb|Mathematica|`. Files endig with ``.py`` are run in :math:`\verb|SageMath|` and diles endig with ``.m2`` are run in :math:`\verb|Macaulay2|`. A file with ending ``.ipnynb`` is a Jupyter Notebook that is executed with a :math:`\verb|SageMath|` kernel.


Computing the Polytope from Payoffs
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

We provide code to compute the correlated equilbrium polytope and the correlated equilibrium cone of a fixed :math:`(d_1 \times \dots \times d_n)`-game with payoff tensor in :math:`X` in :math:`\verb|SageMath|`. 
First, download the file :download:`correlated_polytope_cone.py <correlated_polytope_cone.py>` and place it in the same folder as your Jupyter Notebook for SageMath. An example of a :math:`(2 \times 3)`-game can then be computed by the lines

.. code-block:: python

   from correlated_polytope_cone import *
   D_i = [2,3]
   X = [
       [9,4],     # X_{11} = [X_{11}^(1), X_{11}^(2)]
       [4,0],     # X_{12} = [X_{12}^(1), X_{12}^(2)]
       [6,2],     # X_{13} = [X_{13}^(1), X_{13}^(2)]
       [5,5],     # X_{21} = [X_{21}^(1), X_{21}^(2)]
       [3,8],     # X_{22} = [X_{22}^(1), X_{22}^(2)]
       [9,7]      # X_{23} = [X_{23}^(1), X_{23}^(2)]
   ]
   P = correlated_equilibrium_polytope(D_i, X, base_ring = QQ)
   C = correlated_equilibrium_cone(D_i, X, base_ring = QQ)

The input :math:`D\_i = [d_1,\dots,d_n]` is the type of the game, e.g. ``D_i = [2,3]`` for a :math:`(2 \times 3)`-game as in the example above. ``X`` above corresponds to the payoff table.

.. |s12| replace:: 
   :math:`s_1^{(2)}`

.. |s22| replace:: 
   :math:`s_2^{(2)}`

.. |s32| replace:: 
   :math:`s_3^{(2)}`

.. |s11| replace:: 
   :math:`s_1^{(1)}`

.. |s21| replace:: 
   :math:`s_2^{(1)}`



+------------+-----+-----+-----+-----+
|            |   **Player 2**        |
+            +-----+-----+-----+-----+
|            |     ||s12|||s22|||s32||
+------------+-----+-----+-----+-----+ 
|**Player 1**||s11||(9,4)|(4,0)|(6,2)|
+            +-----+-----+-----+-----+
|            ||s21||(5,5)|(3,8)|(9,7)|
+------------+-----+-----+-----+-----+


|

Section 4: The Region of Full-Dimensionality
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

- :download:`dimensions2x2.nb <dimensions2x2.nb>`

- :download:`dimensions2x3.nb <dimensions2x3.nb>`


Section 5: Combinatorial Types of Correlated Equilibrium Polytopes
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

- :download:`orientedMatroidStrata2x2.m2 <orientedMatroidStrata2x2.m2>`

- :download:`orientedMatroidStrata2x3.m2 <orientedMatroidStrata2x3.m2>`

- :download:`orientedMatroidStrata2x2x2.m2 <orientedMatroidStrata2x2x2.m2>`

Proof of Theorem 5.9
""""""""""""""""""""

- :download:`combinatorialTypes2x3.ipynb <combinatorialTypes2x3.ipynb>`

- :download:`combinatorialTypes2x3.nb <combinatorialTypes2x3.nb>`

- :download:`strataReps2x3.txt <strataReps2x3.txt>`


Code for Random Sampling Combinatorial Types of Polytopes
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

- :download:`2xn-random-sampling.zip <2xn-random-sampling.zip>`

- :download:`2x2x2-random-sampling.zip <2x2x2-random-sampling.zip>`


|

| **Credits**
| Project page created: 14/09/2022
| Project contributors: Marie-Charlotte Brandenburg, Benjamin Hollering, and Irem Portakal
| Software used: SageMath (Version 9.6), Mathematica (Version 13.0), Macaulay2 (Version 1.20)
| System setup used: MacBook Pro with macOS Monterey 12.5.1, Processor 3,1 GHz Quad-Core Intel Core i7, Memory 16 GB 2133 MHz LPDDR3, Graphics Intel HD Graphics 630 1536 MB.
| Corresponding authors of this page: Marie-Charlotte Brandenburg (marie.brandenburg@mis.mpg.de) and Benjamin Hollering (benjamin.hollering@mis.mpg.de)
