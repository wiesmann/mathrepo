===============================================
Minimality of tensors of fixed multilinear rank
===============================================



| This page contains computations related to the paper:
| Alexander Heaton, Khazhgali Kozhasov, and Lorenzo Venturello: Minimality of tensors of fixed multilinear rank
| In: Linear and multilinear algebra, Vol. not yet known, pp. not yet known
| DOI: `10.1080/03081087.2022.2062274 <https://dx.doi.org/10.1080/03081087.2022.2062274>`_ ARXIV: https://arxiv.org/abs/2007.08909 CODE: https://mathrepo.mis.mpg.de/tensorminimality



In this article, we discover a geometric property of the space of tensors of fixed multilinear (Tucker) rank. Namely, we show that real tensors of fixed multilinear rank form a minimal submanifold of the Euclidean space of all tensors of given format endowed with the Frobenius inner product.

Here, we present a computation which verifies a (very) special case of our main theorem. Namely, we compute the mean curvature vector field of a submanifold of Euclidean space, showing that it is everywhere vanishing. We also compute the mean curvature vector field of a related submanifold of interest in statistics, formed by slicing the minimal sub manifold with an affine hyperplane. Since this submanifold is no longer minimal, we visualize its mean curvature vector field, shown below.

.. image:: tensors-mean-curvature-field.png

The Jupiter file making these computations can be seen by clicking the link below.

.. toctree::
	:maxdepth: 1
	:glob:

	tensor-minimality


The Jupyter notebook can be downloaded :download:`here <tensor-minimality.ipynb>`.



Project page created: 25/11/2020

Code contributors: Alexander Heaton

Jupyter Notebook written by: Alexander Heaton, 25/11/2020

Project contributors: Alexander Heaton

Software used: SageMath 9.1.

Corresponding author of this page: Alexander Heaton


