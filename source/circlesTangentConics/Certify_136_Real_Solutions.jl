using HomotopyContinuation
using LinearAlgebra

@var s, t, r # Circle centered at (s, t) with radius r
@var u[1:3], v[1:3] # Three points of tangency (u[1], v[1]), ..., (u[3], v[3])
@var a[1:6], b[1:6], c[1:6] # Three fixed conics with coefficients defined by a[1], ..., a[6]; ... ; c[1], ..., c[6]

#=
Set up the polynomial system defining tritangent circles
=#

f_1 = (u[1] - s)^2 + (v[1] - t)^2 - r # the point (u[1], v[1]) lies on the circle

f_2 = (u[2] - s)^2 + (v[2] - t)^2 - r # the point (u[2], v[2]) lies on the circle

f_3 = (u[3] - s)^2 + (v[3] - t)^2 - r # the point (u[3], v[3]) lies on the circle

f_4 = (400141104595769//2302676434480590430)*u[1]^2 + (5537854491843451//2305843009213693952)*u[1]*v[1] + (2379998783885947//288230376151711744)*v[1]^2 + (-5883336424977557//288230376151711744)*u[1] + (-5057485722682341//36028797018963968)*v[1] + (2686777020175459//4503599627370496)
# the point (u[1], v[1]) lies on the conic

f_5 = (2326975324861901//144115188075855872)*u[2]^2 + (-7017759077361941//576460752303423488)*u[2]*v[2] + (5286233514864229//2305843009213693952)*v[2]^2 + (3536130883475143//18014398509481984)*u[2] + (-5331739727004679//72057594037927936)*v[2] + (5373554039379455//9007199254740992)
# the point (u[2], v[2]) lies on the conic

f_6 = (6288284117996449//576460752303423488)*u[3]^2 + (-8069853070614251//288230376151711744)*u[3]*v[3] + (1293970525023733//72057594037927936)*v[3]^2 + (-1453444402131837//9007199254740992)*u[3] + (7458321785480773//36028797018963968)*v[3] + (2686777019781135//4503599627370496)
# the point (u[3], v[3]) lies on the conic

f_7 = det([differentiate(f_1, [u[1], v[1]]) differentiate(f_4, [u[1], v[1]])]) # Circle and conic are tangent at (u[1], v[1])

f_8 = det([differentiate(f_2, [u[2], v[2]]) differentiate(f_5, [u[2], v[2]])]) # Circle and conic are tangent at (u[2], v[2])

f_9 = det([differentiate(f_3, [u[3], v[3]]) differentiate(f_6, [u[3], v[3]])]) # Circle and conic are tangent at (u[3], v[3])

F = System([f_1, f_2, f_3, f_4, f_5, f_6, f_7, f_8, f_9], variables = [u[1], v[1], u[2], v[2], u[3], v[3], s, t, r])

S = solve(F)

certify(F, S)
