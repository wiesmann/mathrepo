..
  spaceSexticCurves_tritangents:

######################################################
Reconstruction of Space Sextics from their Tritangents
######################################################

We present a Magma script for reconstructing a space sextic from its tritangents, or rather its Steiner System, as described in

T. O. Celik, A. Kulkarni, Y. Ren, M. Sayyary: *Tritangents and their space sextics*, in preparation (2018).

The script is comprised of four parts:

* :download:`main.m <main.m>`
  The main script, in which the curve is defined and all top-level functions are called. Also contains functions for reconstructing the defining quadric of the space sextics.

* :download:`genericTritangents.m <genericTritangents.m>`
  Contains functions for computing the tritangents of a space sextic, as explained in the previous parts.

* :download:`steinerSolver.m <steinerSolver.m>`
  Contains functions for computing the Steiner system of a space sextic.

* :download:`cayleySpaceCode.m <cayleySpaceCode.m>`
  Contains functions for computing the defining cubic of the space sextic.

As of now, we assume the quadric to be :math:`x_0x_2-x_1x_3`. To use the script, simply change the definition of the curve to your liking. The default curve is

.. code-block:: none

                kbase := GF(7);
                P3base<x0,x1,x2,x3> := ProjectiveSpace(kbase,3);

                // **********************************************************************
                // Curve defined.

                // The smooth quadric (DO NOT CHANGE)
                q := x0*x2-x1*x3;

                // A transformed Cayley cubic.
                f := x0*x1*x2 + x0*x1*x3 + x0*x2*x3 + x1*x2*x3;
                f := 1/4*x0*x1*x2 + x0*x1*x3 + x0*x2*x3 + x1*x2*x3;
                f := Evaluate(f, [x0-x1, x0-2*x2+x1, x2-x3 + 5*x0, x3-4*x1 + 11*x0]);

                // The curve.
                C := Curve(Scheme(P3base, [q,f]));
                assert IsNonSingular(C);

which yields

.. code-block:: none

                bash-3.2$ magma main.m
                Magma V2.23-1     Tue May 29 2018 14:31:09 on mb019    [Seed = 407767936]
                Type ? for help.  Type <Ctrl>-D to quit.

                Loading file "main.m"
                Loading "genericTritangents.m"
                Loading "steinerSolver.m"

                7140
                7139
                [...]
                1
                basis of quadrics:
                [
                  x0^2,
                  x0*x1,
                  x1^2,
                  x0*x2,
                  x1*x2,
                  x2^2,
                  x0*x3,
                  x1*x3,
                  x2*x3,
                  x3^2
                ]
                intersection of steiner subspaces:
                Vector space of degree 10, dimension 1 over GF(7^20)
                Echelonized basis:
                (0 0 0 1 0 0 0 6 0 0)
                Quadric equation:
                x0*x2 + 6*x1*x3
                Loading "cayleySpaceCode.m"
                Subspace 1 computed.
                Subspace 2 computed.
                [...]
                Subspace 255 computed.
                Check if the reconstruction was successful: true
                > Ctest;
                Curve over GF(7^20) defined by
                x0^3 + x0^2*x1 + 5*x1^3 + 5*x1^2*x2 + x1*x2^2 + 3*x0^2*x3 + 4*x0*x1*x3 + 4*x1^2*x3 + 4*x1*x2*x3 + x2^2*x3 +
                    x0*x3^2 + 6*x1*x3^2 + 6*x2*x3^2,
                x0^2*x2 + 6*x0*x1*x3,
                x0*x1*x2 + 6*x1^2*x3,
                x0*x2^2 + 6*x1*x2*x3,
                x0*x2*x3 + 6*x1*x3^2


Note: The countdown from 7140 to 1 happens during the construction of the Steiner system from the :math:`\binom{120}{2}=7140` pairs of tritangents (which is the computationally hardest part of the script), and the count from 1 to 255 happens during the construction of the :math:`|JC[2]\setminus \{0\}|=255` spaces of quadrics :math:`\mathbb PV_{C,\alpha}`.
