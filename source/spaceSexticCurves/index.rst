=========================================
Space Sextic Curves and Their Tritangents
=========================================

.. image:: tritangentsComplexRealAndTotallyReal.png

A generic space sextic is the intersection of a smooth quadric and a cubic surface. It is a canonical curve of genus 4, and it has 120 complex tritangent planes, one for each odd theta characteristic. We present various experiments and their results surrounding the reality issues that arise in the context of this classical problem. A more detailed summary can be found in the following article:

| Avinash Kulkarni, Yue Ren, Mahsa Sayyary Namin, and Bernd Sturmfels: Real space sextics and their tritangents
| In: ISSAC '18 proceedings of the 43rd international symposium on symbolic and algebraic computation ; New York, USA, July 16-19, 2018
| New York : ACM, 2018. - P. 247-254
| MIS-Preprint: `81/2017 <https://www.mis.mpg.de/publications/preprints/2017/prepr2017-81.html>`_ DOI: `10.1145/3208976.3208977 <https://dx.doi.org/10.1145/3208976.3208977>`_ ARXIV: https://arxiv.org/abs/1712.06274 CODE: https://mathrepo.mis.mpg.de/spaceSexticCurves



.. toctree::
   :maxdepth: 2

   tritangentsGeneric.rst
   tritangentsDelPezzo.rst
   tritangentsToCurve.rst


Project contributors: Avinash Kulkarni, Yue Ren, Mahsa Sayyary Namin, and Bernd Sturmfels
   
Software used: Magma, Mathematica