needsPackage("Points");

-- The range of numbers of points r in P^n with Hilbert regularity d and nontrivial gap (if Igc=>true, otherwise all with finite saturation gap)
nontrivGapRange = method(Options => {Igc => true});
nontrivGapRange (ZZ,ZZ) := o -> (n,d) -> (
    rmin := if o.Igc == true then
        ceiling( ((n+1)*binomial(n+d,n) - binomial(n+d+1,n) + 1)/n )
    else
        binomial(n+d-1,n)+1;
    rmax := binomial(n+d,n) - (n+1);
    return (rmin,rmax);
);

-- The Hilbert regularity of a general set of r points in P^n
regH = (n,r) -> (
    t := 1;
    while binomial(n+t,n) < r do t = t+1;
    return t;
);

-- The alternating sum from Fröberg's conjecture
alternatingSum = (n,r,t) -> (
    d := regH(n,r);
    g := binomial(d+n,n) - r;
    return sum(0..floor(t/d), k -> (-1)^k * binomial(t-k*d+n,n) * binomial(g,k));
);

-- The Hilbert function of the chopped ideal of r general points in P^n according to the Expected Syzygy Conjecture (ESC)
expectedHF = (n,r,t) -> (
    d = regH(n,r);
    e = expectedGapSize(n,r);
    return if t < d+e then alternatingSum(n,r,t) else r;
);

-- The length of the saturation gap, i.e. the least e>0 such that (I_<d>)_d+e = I_d+e
expectedGapSize = (n,r) -> (
    if r == 1 or (n,r) == (2,4) then return 1;
    d := regH(n,r);
    if r >= binomial(n+d,n)-n then return infinity;
    e := 1;
    while alternatingSum(n,r,d+e) > r do e = e+1;
    return e;
);

-- Checks if the given ideal has the expected Hilbert function according to (ESC)
-- This method assumes that I is a 1-dim'l graded ideal of degree r
-- Optional argument IsChopped: Whether I==I_<d>. Ideals from Points.m2 are not chopped, but those from our DB are.
verifyESC = method(Options => {IsChopped => false});
verifyESC (Ideal,ZZ) := o -> (I,r) -> (
    S := ring I;
    n := numgens(S)-1;
    d := regH(n,r);
    e := expectedGapSize(n,r);
    if e == infinity then throw "Expected saturation gap is infinite!";

    gs := first entries gens I;
    if min(apply(gs, f -> (degree f)_0)) != d then return false; -- Check that I has generic HF up to d-1

    -- The case e=1, only need to check that IGC holds (correct number of generators in degree d, none in degree d+1)
    if e == 1 then (
        mings := first entries mingens I;
        count := 0;
        for f in mings do if (degree f)_0 == d then count = count + 1 else return false;
        if not count == binomial(n+d,n) - r then return false;
        if o.IsChopped == false then return true;
        return #(first entries super basis(d+1,I)) == binomial(n+d+1,n) - r; -- Calculate dimension of (I_<d>)_{d+1}
    );

    -- The case e>1, need to calculate the Hilbert function by expanding the Hilbert series
    J := if o.IsChopped then I else ideal select(gs, f -> (degree f)_0 == d);
    hs := hilbertSeries(J, Order=>d+e+1);
    T := (ring hs)_0;
    for t from d to d+e do
        if coefficient(T^t, hs) != expectedHF(n,r,t) then return false;

    return true;
);

loadPoints = method();
loadPoints String := file -> (
    ls := lines get file;
    S := value ls#0;
    use S;
    r := value ls#1;
    Z := for i from 2 to r+1 list value ls#i;
    J := ideal for i from r+2 to #ls - 1 list value ls#i;
    return (Z,J);
);


-- This is how the files on KEEPER are named.
padInt = (n,l) -> (str := toString(n); while #str < l do str = "0"|str; return str;);
filename = (n,r) -> concatenate("P",padInt(n,2),"/d",padInt(regH(n,r),2),"_r",padInt(r,4),".txt");

-- Saves the list of points pts and the chopped ideal of I (in this degree) in a file
savePoints = method()
savePoints (List,Ideal,String) := (pts,I,file) -> (
    r := #pts;
    file << toExternalString ring I << endl;
    file << toExternalString r << endl;
    for pt in pts do (file << toExternalString(pt) << endl);
    d := regH(#(pts_0) - 1, r);
    gs := select(first entries gens I, f -> (degree f)_0 == d);
    for g in gs do file << toExternalString(g) << endl;
    file << close;
)

toPointsMat = Z -> matrix transpose Z;
fromPointsMat = mat -> transpose entries mat;

-- Two methods to compute the ideal of Z (with ZZ coordinate) in S
f1 = (Z,S) -> points(sub(Z,S));
f2 = (Z,S) -> ideal last projectivePoints(Z,S,VerifyPoints=>false);

testCase = method(Options => {IdealMethod => f1, p => 1009})
testCase (ZZ,ZZ) := o -> (n,r) -> (
    S := ZZ/o.p[x_0..x_n];
    d := regH(n,r);

    identStr := ("(n=" | toString(n) | ",d=" | toString(d) | ",r=" | toString(r) | ")");
    print(identStr | " -> expected gap = " | expectedGapSize(n,r));

    while true do (
        Z := random(ZZ^(n+1),ZZ^r, Height=>o.p);
        elapsedTime (I := o.IdealMethod(Z,S));
        print(identStr | " ideal calculated");
        if verifyESC(I,r, IsChopped=>false) then (
            print(identStr | " valid!");
            return (fromPointsMat Z,I);
        );
        print(identStr | " invalid, trying again");
    );
)

verifyCertificate = method();
verifyCertificate String := file -> (
    (Z,J) := loadPoints(file);
    S := ring J;
    r := #Z;
    for z in Z do assert(
        sub(J,sub(matrix{z},S)) == 0 -- Check if J \subseteq I(Z) by verifying Z \subseteq V(J)
    );
    print("Point evaluation done");
    assert verifyESC(J,r,IsChopped=>true);    -- Check if J has expected HF (in certain degrees), which implies that J = I(Z)_<d>
    print("ESC verified for r=" | toString(r) | " points in P^" | toString(numgens(S) - 1));
);

verifyDirectory = method();
verifyDirectory String := dir -> (
    for file in sort(readDirectory(dir)) do (
        if not substring(#file-4,4,file) == ".txt" then continue;
        print("Verifying " | dir | "/" | file);
        verifyCertificate(dir | "/" | file);
    );
    print("All files in " | dir | " verified!")
);