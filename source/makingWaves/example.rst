========================================
Example computation
========================================

We replicate here Example 6.4 using the code described in :doc:`implementation`.

We begin by defining our polynomial ring and matrix

.. code-block:: macaulay2

  i1 : load "makingWaves.m2";

  i2 : R = QQ[x_1..x_4];

  i3 : A = matrix{{x_1,x_2,x_3},{x_2,x_1,x_4},{x_3,x_4,x_1}}

  o3 = | x_1 x_2 x_3 |
       | x_2 x_1 x_4 |
       | x_3 x_4 x_1 |

               3       3
  o3 : Matrix R  <--- R

This corresponds to the PDE

.. math::
   
  \frac{\partial \phi_1}{\partial x_1} + \frac{\partial \phi_2}{\partial x_2} + \frac{\partial \phi_3}{\partial x_3} = 0

  \frac{\partial \phi_1}{\partial x_2} + \frac{\partial \phi_2}{\partial x_1} + \frac{\partial \phi_3}{\partial x_4} = 0

  \frac{\partial \phi_1}{\partial x_3} + \frac{\partial \phi_2}{\partial x_4} + \frac{\partial \phi_3}{\partial x_1} = 0

for some distribution :math:`\phi \colon \mathbb{R}^4 \to \mathbb{C}^3`.

We can check that :math:`\mathcal{P}_A^0 = \mathcal{P}_A^1 = \emptyset` using ``wavePairs(A,0)`` and ``wavePairs(A,1)``.

The first interesting case occurs when :math:`r = 2`.

.. code-block:: macaulay2

  i4 : I = wavePairs(A, 2)

                                                                                                                                                                                                                                                                                                                                2                 2                          2      2                                                          2      2                                                          2      2                          2      2             2      2      3   2      3      2   3      2      2
  o4 = ideal (p   z  + p   z , p   z  + p   z , p   z  + p   z , p   z  + p   z  + p   z , p   z  - p   z , p   z  + p   z , p   z  - p   z , p   z  + p   z  - p   z , p   z  - p   z , p   z  - p   z  - p   z , p   z  + p   z , p   z  - p   z , p   p    + p   p   , p   p    + p   p   , p   p    - p   p   , p   p    - p    - p   p    + p   , p   p    + p   p   , p    - p    - p   p   , p   p    - p   p   , p   p    - p   p   , p    - p    - p   p   , p   p    - p   p   , p   p    + p   p   , p    - p   , p   p    - p   p   , p    - p   , z z z , z z  + z z  - z , z z  - z  + z z , z  - z z  - z z )
               1,3 3    2,3 2   0,3 3    2,3 1   0,3 2    1,3 1   0,3 1    1,3 2    2,3 3   1,2 3    2,3 1   1,2 2    1,3 1   0,2 3    2,3 2   0,2 2    1,2 1    2,3 3   0,2 1    1,3 1   0,1 3    1,2 1    1,3 2   0,1 2    2,3 2   0,1 1    2,3 1   1,2 2,3    0,3 2,3   0,2 2,3    1,3 2,3   1,2 1,3    0,3 1,3   0,2 1,3    1,3    0,1 2,3    2,3   0,1 1,3    1,3 2,3   0,3    1,3    0,1 2,3   0,2 0,3    0,3 1,3   0,1 0,3    0,3 2,3   1,2    1,3    0,1 2,3   0,2 1,2    0,3 1,3   0,1 1,2    0,3 2,3   0,2    1,3   0,1 0,2    1,3 2,3   0,1    2,3   1 2 3   1 3    2 3    3   1 2    2    2 3   1    1 2    1 3

                QQ[p   ..p   , p   , p   , p   , p   , z ..z ]
                    0,1   0,2   1,2   0,3   1,3   2,3   1   3
  o4 : Ideal of ----------------------------------------------
                        p   p    - p   p    + p   p
                         1,2 0,3    0,2 1,3    0,1 2,3

  i5 : netList decompose I

       +----------------------------------------------------------------------+
  o5 = |ideal (z , z  - z , p   , p    + p   , p    + p   , p    - p   , p   )|
       |        3   1    2   2,3   0,3    1,3   1,2    1,3   0,2    1,3   0,1 |
       +----------------------------------------------------------------------+
       |ideal (z , z  + z , p   , p    - p   , p    - p   , p    - p   , p   )|
       |        3   1    2   2,3   0,3    1,3   1,2    1,3   0,2    1,3   0,1 |
       +----------------------------------------------------------------------+
       |ideal (z , z  - z , p   , p    + p   , p    - p   , p   , p    - p   )|
       |        2   1    3   1,3   0,3    2,3   1,2    2,3   0,2   0,1    2,3 |
       +----------------------------------------------------------------------+
       |ideal (z , z  + z , p   , p    - p   , p    + p   , p   , p    - p   )|
       |        2   1    3   1,3   0,3    2,3   1,2    2,3   0,2   0,1    2,3 |
       +----------------------------------------------------------------------+
       |ideal (z  - z , z , p    + p   , p   , p   , p    - p   , p    + p   )|
       |        2    3   1   1,3    2,3   0,3   1,2   0,2    2,3   0,1    2,3 |
       +----------------------------------------------------------------------+
       |ideal (z  + z , z , p    - p   , p   , p   , p    + p   , p    + p   )|
       |        2    3   1   1,3    2,3   0,3   1,2   0,2    2,3   0,1    2,3 |
       +----------------------------------------------------------------------+

The variety consists of six points, each of which corresponds to a family of wave solutions. For example, the first one yields

.. math::
   \phi(x_1,\dotsc,x_4) = \delta(x_1 - x_2, x_3 - x_4) \begin{bmatrix} 1 \\ 1 \\ 0 \end{bmatrix}

for any distribution :math:`\delta \colon \mathbb{R}^2 \to \mathbb{C}`.

If we perform the computation in the affine patch where :math:`p_{2,3} = 1`, only four of the solutions survive.

.. code-block:: macaulay2

  i6 : netList decompose wavePairs(A, 2, Patch => {2,3})

       +-----------------------------------------------------------------------+
  o6 = |ideal (z , z  - z , p    - 1, p   , p    + 1, p    - 1, p   , p    - 1)|
       |        2   1    3   2,3       1,3   0,3       1,2       0,2   0,1     |
       +-----------------------------------------------------------------------+
       |ideal (z , z  + z , p    - 1, p   , p    - 1, p    + 1, p   , p    - 1)|
       |        2   1    3   2,3       1,3   0,3       1,2       0,2   0,1     |
       +-----------------------------------------------------------------------+
       |ideal (z  - z , z , p    - 1, p    + 1, p   , p   , p    - 1, p    + 1)|
       |        2    3   1   2,3       1,3       0,3   1,2   0,2       0,1     |
       +-----------------------------------------------------------------------+
       |ideal (z  + z , z , p    - 1, p    - 1, p   , p   , p    + 1, p    + 1)|
       |        2    3   1   2,3       1,3       0,3   1,2   0,2       0,1     |
       +-----------------------------------------------------------------------+
    
