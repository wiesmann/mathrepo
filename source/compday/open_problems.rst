=============
Open Problems
=============

.. compday_openproblems:

.. |br| raw:: html

   <br />


Explicitly Stated Open Problems in Articles by Bernd Sturmfels
==============================================================

  - :math:`1704.01910` |br|
    **Geometry of Log-Concave Density Estimation** |br|
    **Problem 5.8:** Which triangulations arise from log-concave MLE with unit weights?
  - :math:`1703.01660` |br|
    **Sixty-Four Curves of Degree Six** |br|
    **Conjecture 5.8:** Real sextics in :math:`\mathbb{P}^2` have between 12 and 306 real bitangents.
  - :math:`1510.08797` |br|
    **Convexity in Tree Spaces** |br|
    **Problem 8:** Are geodesic triangles in orthant spaces always closed?
  - :math:`1612.01129` |br|
    **Algebraic Identifiability of Gaussian Mixtures** |br|
    **Conjecture 2:** Moment varieties of bivariate Gaussian mixtures are identifiable.
  - :math:`1606.02253` |br|
    **How to Flatten a Soccer Ball** |br|
    **Problem 1:** Map the unit 3-ball polynomially onto an arbitrary convex polygon.
  - :math:`1601.06574` |br|
    **Real Rank Geometry of Ternary Forms** |br|
    **Conjecture 4.3:** The real rank boundary for ternary quartics has degree :math:`6+27+51`.
  - :math:`1504.08049` |br|
    **Decomposing Tensors into Frames** |br|
    **Conjecture 3.3:** The fradeco variety for binary forms is determinantal.
  - :math:`1402.5651` |br|
    **Tropicalization of Del Pezzo Surfaces** |br|
    **Conjecture 5.3:** The 270 trinomials generators are a tropical basis for the universal Cox ideal of cubic surfaces.
  - :math:`1412.6185` |br|
    **Exponential Varieties** |br|
    **Problem 7.5:** Determine the exponential variety of inverse Hankel matrices.
  - :math:`1303.1132` |br|
    **Tropicalization of classical moduli spaces** |br|
    **Conjecture 3.5:** The universal abelian surface in :math:`\mathbb{P}^3 \times \mathbb{P}^8` is defined by
    93 polynomials, nine of bidegree :math:`(4,2)` and 84 of bidegree :math:`(3,3)`.

