==========
Problem 18
==========

.. compday_problem18:

Compute the tropical convex hull of the  Hilbert matrix, for various :math::`n`.

Solution
========

In *Polymake*:

.. code-block:: perl

                sub Hilbert{
                  my $n = $_[0];
                  my $h = new Matrix(zero_matrix($n,$n));
                  for(my $i=0; $i<$n; $i++){
                    for(my $j=0; $j<$n; $j++){
                      $h->elem($i,$j)=new Rational(1,($i+$j+1)); 
                    }
                  }
                  return new tropical::Polytope<Min>(POINTS=>$h);
                }
                
.. image:: 18.png
