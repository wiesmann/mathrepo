==========
Problem 10
==========

.. compday_problem10:

The tropical Grassmannian :math:`{\rm Gr}(2,6)` is the space of phylogenetic trees with six leaves.
Cueto-Werner described a compactification of :math:`{\rm Gr}(2,6)`.  Can we use  ``cellularSheaves``
to compute tropical homology for both?

