==========
Problem 15
==========

.. compday_problem15:
   
Compute the mixed volume of the following three triangles in :math:`\mathbb{R}^3`:

.. math::

   \begin{matrix}
   T_1 = {\rm conv}((2a,2a,2a),(0,a,a),(1,0,0)), \\
   T_2 = {\rm conv}((2a,2a,2a),(a,0,a),(0,1,0)), \\
   T_3 = {\rm conv}((2a,2a,2a),(a,a,0),(0,0,1)).
   \end{matrix} \qquad \quad {\rm for}\,\,a \in \mathbb{N}.
