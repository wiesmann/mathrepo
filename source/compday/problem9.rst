=========
Problem 9
=========

.. compday_problem9:

Construct all groups of order 60. Which is better for this: *GAP* or *Magma*?

Solution
========

Here is a solution in *Mathematica*:

.. code-block:: mathematica

                Timing[FiniteGroupData[60]]
                Length[FiniteGroupData[60]]

.. image:: 9.png
                
