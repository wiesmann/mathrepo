========================================
Self-dual matroids from canonical curves
========================================

| This page contains auxiliary files to the paper:
| Alheydis Geiger, Sachi Hashimoto, Bernd Sturmfels, Raluca Vlad: Self-dual matroids from canonical curves
|  ARXIV: https://arxiv.org/abs/2212.05910 CODE: https://github.com/sachihashimoto/self-dual

| ABSTRACT: Self-dual configurations of 2n points in a projective space of dimension n-1 were studied by Coble, Dolgachev--Ortland, and Eisenbud--Popescu. We examine the self-dual matroids and self-dual valuated matroids defined by such configurations, with a focus on those arising from hyperplane sections of canonical curves. These objects are parametrized by the self-dual Grassmannian and its tropicalization. We tabulate all self-dual matroids up to rank five and investigate their realization spaces. Following Bath, Mukai, and  Petrakiev, we explore algorithms for recovering a curve from the configuration. A detailed analysis is given for self-dual matroids arising from graph curves.

The code for our project can be found on the git repository <https://github.com/sachihashimoto/self-dual> so that users can easily access the data and track changes to the repository.

This page and the presentation and accessibility of the data are a work in progress. If you have any questions, please email us.

In our paper, we study self-dual matroids, i.e. matroids that are equal to their own dual. E.g. in rank 3, the two self-dual matroids on the set [6] are the uniform matroid, with no nonbases, and the matroid with nonbases {123, 456}. For rank 4, the non-uniform self-dual matroids by nonbases can be found `here <https://github.com/sachihashimoto/self-dual/blob/main/rk4matroidsnonbases>`_. The data of their realization spaces and self-dual realization spaces can be found `in this file <https://github.com/sachihashimoto/self-dual/blob/main/rk4.out>`_ which contains colon-separated values of the line number of the matroid from the previous file, its bases, a Groebner basis for the closure of the realization space, the dimension of the realization space, a Groebner basis of the closure of the self-dual realization space, the dimension fo the self-dual realization space, a boolean saying whether the two spaces are equal, and finally a template for the self-dual realizations of the matroid (to help the user, however this template matrix is still subject to the constraints of the self-dual Groebner basis given). 

This file is meant to be machine readable using the python program `here <https://github.com/sachihashimoto/self-dual/blob/main/prettyprint.py>`_. Sample output of this python program looks like this:

| Enter line number: 1 
| Number of bases: 60 
| Dimension of realization space: 5
| Dimension of self-dual realization space: 4
| Are the spaces equal? false
| Groebner basis for realization space:[x[1]-1,x[2]-1,x[3]-1,x[4]-1,x[5]-1,x[6]-1,x[9]-1,x[10]-1,x[13]-1,x[15],x[16]]
| Groebner basis for self-dual realization space:[x[1]-1,x[2]-1,x[3]-1,x[4]-1,x[5]-1,x[6]-1,x[7]*x[8]*x[11]-x[7]*x[8]*x[12]-x[7]*x[11]*x[12]+x[7]*x[12]+x[8]*x[11]*x[12]-x[8]*x[11],x[9]-1,x[10]-1,x[13]-1,x[15],x[16]]
| Template for ISD matroid:
| [1 0 0 0 1 1 1 1]
| [0 1 0 0 1 1 x[7] x[8]]
| [0 0 1 0 1 1 x[11] x[12]]
| [0 0 0 1 1 x[14] 0 0]]
| Do you want to see the bases? yes/no

We see that specializing the template gives the the self-dual configurations of the matroid (avoiding certain bad values like x[14] = 1, which belong to the closure of the self-dual realization space but not he self-dual realization space) subject to the extra constraint that x[7]*x[8]*x[11]-x[7]*x[8]*x[12]-x[7]*x[11]*x[12]+x[7]*x[12]+x[8]*x[11]*x[12]-x[8]*x[11].

The python program above also parses the data on the rank 5 matroids on 10 elements. The list of 1041 nonuniform rank 5 matroids on 10 elements are available  at `this link <https://github.com/sachihashimoto/self-dual/blob/main/rk5matroids>`_ (they are labeled by line number, but `this file <https://github.com/sachihashimoto/self-dual/blob/main/rk5matroidslabeled>`_ also gives the matroids along with the label). This data was computed using the code `here <https://github.com/sachihashimoto/self-dual/blob/main/get_matroids_510.jl>`_. When available, the analogous data to the rank 4 case (of the realization and self-dual realization space) is available `here <https://github.com/sachihashimoto/self-dual/blob/main/rk5selfdualrealizations>`_. That file can be read in the same manner above, using the python program. 

The data of the Groebner bases for all but 9 of the realization spaces is available `here <https://github.com/sachihashimoto/self-dual/blob/main/rk5allrealizations_nov7>`_. The code to compute this data is `here <https://github.com/sachihashimoto/self-dual/blob/main/rk5optimize.m>`_ (and for the exceptional unframed case, it is `here <https://github.com/sachihashimoto/self-dual/blob/main/rk5unframed.m>`_). For the remaining 9, a heuristic computation was done by intersecting the realization space with hyperplanes, this code is available `here <https://github.com/sachihashimoto/self-dual/blob/main/rk5optimize_hyperplane_realizationonly.m>`_.

Additionally, for almost all matroids with non-empty self-dual realization space, we obtained example self-dual realizations of the matroid using the code `here <https://github.com/sachihashimoto/self-dual/blob/main/specializematrix.m>`_ and variations of it. The sage code `here <https://github.com/sachihashimoto/self-dual/blob/main/verifyspecialization.sage>`_ verifies the output. The point configurations that were defined over the rationals are given in `this file <https://github.com/sachihashimoto/self-dual/blob/main/rationalspec>`_ and the ones over number fields are in `this file <https://github.com/sachihashimoto/self-dual/blob/main/numberfieldspec.out>`_. Using the Macaulay2 code `found here <https://github.com/sachihashimoto/self-dual/blob/main/makebath.m2>`_ we pass genus 6 curves through the rational point configurations, and the genus 6 curves are recorded `here <https://github.com/sachihashimoto/self-dual/blob/main/genus6curves>`_.

The files `here <https://github.com/sachihashimoto/self-dual/blob/main/Thm26Proof>`_ and `here <https://github.com/sachihashimoto/self-dual/blob/main/Thm26ProofOut>`_ give the Maple code for our proof of Theorem 2.6 about the shape of the Cayley Octad map.

We also provide data on graph curves and a census of matroids arising from graph curves in small genus. `This file <https://github.com/sachihashimoto/self-dual/blob/main/matroid-from-graph.m2>`_ contains Macaulay2 code which: inputs a trivalent 3--connected simple graph with genus g and 2g--2 vertices; constructs the corresponding graph curve with genus g and degree 2g--2; intersects the graph curve with a random hyperplane; and outputs the self-dual matroid of rank g--1 on 2g--2 elements from the resulting configuration of 2g--2 points in the hyperplane section. For each considered graph, we ran the algorithm multiple times to make sure the hyperplane was most generic. We considered all such graphs of genus 4 through 7 and we collected the data we obtained `here <https://github.com/sachihashimoto/self-dual/tree/main/graph%20curves>`_; this data is split by genus (see the following files for genus `4 <https://github.com/sachihashimoto/self-dual/blob/main/graph%20curves/matroids-from-graphs-6vertices.md>`_, `5 <https://github.com/sachihashimoto/self-dual/blob/main/graph%20curves/matroids-from-graphs-8vertices.md>`_, `6 <https://github.com/sachihashimoto/self-dual/blob/main/graph%20curves/matroids-from-graphs-10vertices.md>`_, and `7 <https://github.com/sachihashimoto/self-dual/blob/main/graph%20curves/matroids-from-graphs-12vertices.md>`_).

Project page created: 30/11/2022.

Project contributors: Alheydis Geiger, Sachi Hashimoto, Bernd Sturmfels, Raluca Vlad.

Corresponding author of this page: Sachi Hashimoto, `sachi.hashimoto@mis.mpg.de <mailto:sachi.hashimoto@mis.mpg.de>`_.
 
Software used: Magma (V2.26-11), Julia (Version 1.8.3), OSCAR (version 0.11.0), SageMath (version 9.7), Python (3.10.6), Polymake (4.6), Singular (4.3.1), Maple, GNU Parallel, Macaulay2 (version 1.19.1)

Last updated 1/12/22.
