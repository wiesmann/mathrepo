==============
Tropical Bases
==============

.. image:: banner.png

| This website contains auxilliary material to the article:
| Paul Görlach, Yue Ren, and Jeff Sommars: Detecting tropical defects of polynomial equations
| In: Journal of algebraic combinatorics, 53 (2021) 1, p. 31-47
| DOI: `10.1007/s10801-019-00916-4 <https://dx.doi.org/10.1007/s10801-019-00916-4>`_ ARXIV: https://arxiv.org/abs/1809.03350 CODE: https://mathrepo.mis.mpg.de/tropicalBases

The Singular library :download:`tropicalBasis.lib <tropicalBasis.lib>` contains various helper functions for detecting tropical defects of polynomial systems.

.. toctree::
   :maxdepth: 2

   CertificateDelPezzo.rst
   CertificateGaussoids.rst


Project contributors: Paul Görlach, Yue Ren, and Jeff Sommars

Software used: Singular
