=======================================
Four-Dimensional Lie Algebras Revisited
=======================================


| This page contains auxiliary files to the paper:
| Laurent Manivel, Bernd Sturmfels and Svala Sverrisdóttir: Four-Dimensional Lie Algebras Revisited
| ARXIV: http://arxiv.org/abs/2208.14631 CODE: https://mathrepo.mis.mpg.de/Lie4

   
ABSTRACT:   
The projective variety of Lie algebra structures  on a 4-dimensional
vector space has four irreducible components of dimension 11.
We compute their prime ideals in the polynomial ring in 24 variables.
By listing their degrees and Hilbert polynomials, 
we correct  an earlier publication and we
answer a 1987 question  by Kirillov and Neretin.


Verifications of Theorems 1 and 2
==================================
We used `Macaulay2 <http://www.math.uiuc.edu/Macaulay2/>`_ (version 1.20) to verify Theorems 1 and 2 from the paper.

The file :download:`Lie 4 Component <lie4.m2>` includes the explicit generators of the irreducible components :math:`C_1, C_2, C_3, C_4` which are explained in Section 3 of our paper.
We calculate the dimension, degree, Betti numbers and the Hilbert polynomial of each of these component.  
We also verify that our idels for :math:`C_1, C_3, C_4` are prime. 
To show that :math:`C_1` and :math:`C_3` are prime it is enough to run the `isPrime` command in Macaulay2.
To show :math:`C_4` is prime we run 

.. code-block:: macaulay2
		
    # minimalPrimes C4;
    radical C4 == C4

Since we get the output 1 and `true` we see that :math:`C_4` is prime.
Finally we take the intersection of these components to get the radical ideal of :math:`\operatorname{Lie}_4`
and calculate its dimension, degree and Betti numbers.

In the file :download:`C2 prime <C2prime.m2>` we verify that our ideal for :math:`C_2` is prime. 
We do this by representing the birational parametrization of :math:`C_2` mentioned in Section 5 of our paper. 


Project page created: 30/08/2022.

Project contributors: Laurent Manivel, Bernd Sturmfels and Svala Sverrisdóttir.

Corresponding author of this page: Svala Sverrisdóttir, svalasverris@berkeley.edu

Software used: Macaulay2 (version 1.20).
