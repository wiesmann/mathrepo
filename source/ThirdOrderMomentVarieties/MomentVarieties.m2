newPackage(
        "MomentVarieties",
        Version => "1", 
        Date => "28 January 2022",
        Authors => {
            {Name => "Carlos Amendola", 
                Email => "carlos.amendola@tum.de" 
                },
            {Name => "Mathias Drton", 
                Email => "mathias.drton@tum.de" 
                },
            {Name => "Roser Homs", 
                Email => "roser.homs@tum.de" 
                },
            {Name => "Alex Grosdos", 
                Email => "alex.grosdos@tum.de" 
                },
            {Name => "Elina Robeva", 
                Email => "erobeva@math.ubc.ca" 
                }
            },
        Headline => "tools for computing the ideal of second and third order moments in the case of trees",
        PackageExports => {"Graphs","GraphicalModels"}
        )

export {"top2trek",
    "top3trek",
    "modelRing",
    "targetRing",
    "parametrizationData",
    "generatingMat",
    "trekMatricesIdeal",
    "treeModelIdeal",
    "monomialParametrization",
    "equalTrekLengthParametrization",
    "initialTerm",
    "CoefRing", --optional input
    "sVariable", --optional input
    "tVariable", --optional input
    "bVariable", --optional input
    "aVariable", --optional input
    "ExtraOutput", --optional input
    "ssVar", --ring info hashtable
    "ttVar",  --ring info hashtable
    "aaVar", --ring info hashtable
    "bbVar",  --ring info hashtable
    "llVar" --ring info hashtable
    }


modelVariables = local modelVariables
targetVariables = local targetVariables
modelRingData = local modelRingData
targetRingData = local targetRingData
    
toSymbol = (p) -> (
     if instance(p,Symbol) then p
     else
     if instance(p,String) then getSymbol p
     else
     error ("expected a string or symbol, but got: ", toString p))

modelRing = method(Dispatch=>Thing, Options=>{CoefRing=>QQ,sVariable=>"s",tVariable=>"t"})
modelRing Digraph := Ring => opts -> (G) -> (
     if not instance(G,Digraph) then error ("digraph expected");
     if isCyclic G then error ("expected cyclic digraph");
     GG:=topSort G;
     if not GG#digraph===GG#newDigraph then error ("digraph with topological sorting expected");
     n:=#vertices G;
     s:=toSymbol opts.sVariable;
     t:=toSymbol opts.tVariable;
     kk:=opts.CoefRing;
     sL:=flatten for i from 1 to n list for j from i to n list s_(i,j);
     tL:=flatten flatten for i from 1 to n list for j from i to n list for k from j to n list t_(i,j,k);
     R:=kk[sL,tL];
     H:=new MutableHashTable;
     nextvar:=0;
     for i in sL do (H#i=R_nextvar;nextvar=nextvar+1);
     for i in tL do (H#i=R_nextvar;nextvar=nextvar+1);
     R.modelVariables=new HashTable from H;
     D:=new MutableHashTable;
     D#ssVar=s;
     D#ttVar=t;
     R.modelRingData = new HashTable from D; 
     return R;
)

parametrizationData = method(Dispatch=>Thing,Options=>{CoefRing=>QQ,aVariable=>"a"})
parametrizationData Digraph := Sequence => opts -> (G) -> (
    a:=toSymbol opts.aVariable;
    kk:=opts.CoefRing;
    RG:=gaussianRing(G,Coefficients=>kk,pVariableName=>a);
    L:=directedEdgesMatrix RG;
    W:=bidirectedEdgesMatrix RG;
    return(L,W,RG);)

targetRing = method(Dispatch=>Thing, Options=>{CoefRing=>QQ,aVariable=>"a",bVariable=>"b",ExtraOutput=>false})
targetRing Sequence := Ring => opts -> (G,R) -> (
    a:=toSymbol opts.aVariable;
    kk:=opts.CoefRing;
    (L,W,RG):=parametrizationData(G,CoefRing=>kk,aVariable=>a);
    n:=#vertices G;
    b:=toSymbol opts.bVariable;
    aL:=support W;
    bL:=flatten for i to n-1 list b_(i+1,i+1,i+1);
    lL:=support L;
    T:=kk[aL,bL,lL,Weights=>apply(0..(#support L+2*n-1),i->-1),MonomialOrder=>GLex,Global=>false];    
    H:=new MutableHashTable;
    nextvar:=0;
    for i in aL do (H#i=T_nextvar;nextvar=nextvar+1);
    for i in bL do (H#i=T_nextvar;nextvar=nextvar+1);
    for i in lL do (H#i=T_nextvar;nextvar=nextvar+1);    
    T.targetVariables=new HashTable from H;
    D:=new MutableHashTable;
    D#aaVar=a;
    D#bbVar=b;
    D#llVar=RG.gaussianRingData#lVar;
    T.targetRingData = new HashTable from D; 
    if opts.ExtraOutput then return(T,sub(L,T),sub(W,T)) else return(T);
)   

top2trek = (G,i,k) -> (
     if not instance(G,Digraph) then error ("digraph expected");
     if isCyclic G then error ("expected cyclic digraph");
     GG:=topSort G;
     if not GG#digraph===GG#newDigraph then error ("digraph with topological sorting expected");
     tp:=max toList (forefathers(G,GG#map#i)*forefathers(G,GG#map#k));
     if instance(tp,ZZ) then return tp else return 0;)

top3trek = (G,i,l,m) -> (
     if not instance(G,Digraph) then error ("digraph expected");
     if isCyclic G then error ("expected cyclic digraph");
     GG:=topSort G;
     if not GG#digraph===GG#newDigraph then error ("digraph with topological sorting expected");
     tp:=max toList (forefathers(G,GG#map#i)*forefathers(G,GG#map#l)*forefathers(G,GG#map#m));
     if instance(tp,ZZ) then return tp else return 0;)

generatingMat = (G,R) -> (
     n:=#vertices G;
     E1:={};
     E2:={};
     s:=R.modelRingData#ssVar;
     t:=R.modelRingData#ttVar;
     H:=R.modelVariables;
     aux1:=ideal{0_R};
     aux2:=sub(matrix{{},{}},R);        
     for i from 1 to n do for j from i+1 to n do 
     (   if top2trek(G,i,j)==0 then 
	 (  aux1=trim(aux1+ideal{H#(s_(i,j))});
	    for k from 1 to n do aux1=aux1+ideal{H#(t_(toSequence sort{i,j,k}))} 
	 )    
	 else
	 (
	    for k from 1 to n do
            (    if top2trek(G,i,k)==top2trek(G,j,k) then 
	         aux2=aux2|matrix{{H#(s_(toSequence sort{i,k}))},{H#(s_(toSequence sort{j,k}))}};
	    );
            for l from 1 to n do for m from l to n do
            (    if top3trek(G,i,l,m)==top3trek(G,j,l,m) then 
	         aux2=aux2|matrix{{H#(t_(toSequence sort{i,l,m}))},{H#(t_(toSequence sort{j,l,m}))}};
	    );
         );
         E1=append(E1,{i,j,aux1});
         E2=append(E2,{i,j,aux2});
	 aux2=sub(matrix{{},{}},R);
         aux1=ideal{0_R};
     );
     return (E1,E2);)
   

trekMatricesIdeal = (G,R) -> (
    (E1,E2):=generatingMat(G,R);
    I:=ideal{0_R};
    for e1 in E1 do I=I+e1_2;
    I=trim I;
    for e2 in E2 do I=I+minors(2,e2_2);
    return I;)


monomialParametrization = (G,R,T) -> (
    (L,W,RG):=parametrizationData(G);
    (L,W)=(sub(L,T),sub(W,T));
    --compute the image of gens R via adapted Tucker product
    n:=numcols L;
    Last:=sum(for i to n-1 list L^i);
    M:=new MutableList from apply(0..(#gens R-1),i->0);
    --images of s_(i,j)
    count:=0;
    for i1 to n-1 do for i2 from i1 to n-1 do 
    (  for j1 to n-1 do for j2 from j1 to n-1 do 
       (   M#count=M#count+W_(j1,j2)*Last_(j1,i1)*Last_(j2,i2));
       M#count=leadTerm M#count;
       count=count+1;
    );
    --diagonal tensor of error 3-moments as list of mutable matrices
    E:={};
    for i to n-1 do E=append(E,mutableMatrix(T,n,n));
    b:=T.targetRingData#bbVar;
    H:=T.targetVariables;
  --  for i to n-1 do E_i_(i,i)=H#(b_(i,i,i));
    for i to n-1 do E_i_(i,i)=H#(b_(i+1,i+1,i+1));
    --images of t_(i,j,k)
    for i1 to n-1 do for i2 from i1 to n-1 do for i3 from i2 to n-1 do
    (  for j1 to n-1 do for j2 from j1 to n-1 do for j3 from j2 to n-1 do 
       (   M#count=M#count+E_j1_(j2,j3)*Last_(j1,i1)*Last_(j2,i2)*Last_(j3,i3));
       M#count=leadTerm M#count;
       count=count+1;
    );
    --parametrization
    return map(T,R,toList M);)


initialTerm = (f) -> (
    n:=degree leadTerm f;
    L:={};
    for m in terms f do (if degree m===n then L=append(L,m));
    sum L
    )


equalTrekLengthParametrization = (G,R,T) -> (
    (L,W,RG):=parametrizationData(G);
    (L,W)=(sub(L,T),sub(W,T));
    --compute the image of gens R via adapted Tucker product
    n:=numcols L;
    Last:=sum(for i to n-1 list L^i);
    M:=new MutableList from apply(0..(#gens R-1),i->0);
    --images of s_(i,j)
    count:=0;
    for i1 to n-1 do for i2 from i1 to n-1 do 
    (  for j1 to n-1 do for j2 from j1 to n-1 do 
       (   M#count=M#count+W_(j1,j2)*Last_(j1,i1)*Last_(j2,i2));
       M#count=initialTerm M#count;
       count=count+1;
    );
    --diagonal tensor of error 3-moments as list of mutable matrices
    E:={};
    for i to n-1 do E=append(E,mutableMatrix(T,n,n));
    b:=T.targetRingData#bbVar;
    H:=T.targetVariables;
  --  for i to n-1 do E_i_(i,i)=H#(b_(i,i,i));
    for i to n-1 do E_i_(i,i)=H#(b_(i+1,i+1,i+1));
    --images of t_(i,j,k)
    for i1 to n-1 do for i2 from i1 to n-1 do for i3 from i2 to n-1 do
    (  for j1 to n-1 do for j2 from j1 to n-1 do for j3 from j2 to n-1 do 
       (   M#count=M#count+E_j1_(j2,j3)*Last_(j1,i1)*Last_(j2,i2)*Last_(j3,i3));
       M#count=initialTerm M#count;
       count=count+1;
    );
    --parametrization
    return map(T,R,toList M);)


treeModelIdeal = (G,R,T) -> ( 
    f:=monomialParametrization(G,R,T);
    ker f)
