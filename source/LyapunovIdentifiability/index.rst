=====================================================================
Identifiability in Continuous Lyapunov Models
=====================================================================

| Here may you find the supplementary codes for the paper: 
| Philipp Dettling, Roser Homs, Carlos Améndola, Mathias Drton, Niels Richard Hansen: 
| Identifiability in continuous Lyapunov Models
| ARXIV: https://arxiv.org/abs/2209.03835
| CODE: https://mathrepo.mis.mpg.de/LyapunovIdentifiability

*Abstract of the paper*: 
The recently introduced graphical continuous Lyapunov models provide a new approach to statistical
modeling of correlated multivariate data.  The models view each observation as a one-time cross-sectional
snapshot of a multivariate dynamic process in equilibrium.  The covariance matrix for the data is obtained
by solving a continuous Lyapunov equation that is parametrized by the drift matrix of the dynamic process.
In this context, different statistical models postulate different sparsity patterns in the drift matrix, 
and it becomes a crucial problem to clarify whether a given sparsity assumption allows one to uniquely 
recover the drift matrix parameters from the covariance matrix of the data.  We study this identifiability
problem by representing sparsity patterns by directed graphs.  Our main result  proves that the drift matrix
is globally identifiable if and only if the graph for the sparsity pattern is simple (i.e., does not contain 
directed two-cycles).  Moreover, we present a necessary condition for generic identifiability and provide a 
computational classification of small graphs with up to 5 nodes.

In this repository you will find the code used to compute all the examples in the paper.

.. toctree::
   :maxdepth: 2

   Introduction.rst
   Simplecyclic.rst
   Examples.rst

If you want to run the code yourself, you may download the following *Macaulay2* file:

:download:`LyapunovModel <LyapunovModel.m2>`  

The *Macaulay2* code proving global identifiability of simple cyclic graphs can be downloaded here:

:download:`SimpleGraphID <SimpleGraphsASigma.m2>`  

:download:`SimpleGraphIDkernel <CyclicGraphsKernel.m2>`   

The *Mathematica* and *Macaulay2* code investigating generic identifiability and non-identifiability of non-simple graphs can be downloaded here. Additionally,
we provide the list of non-identifiable graphs up to 5 nodes as txt-file.

:download:`NonSimpleGenericID <GenericIDNumeric.nb>`

:download:`NonSimpleNonID <NonSimple5NonIdentifiable.m2>`

:download:`NonIDGraphlist5 <graphlist.txt>`

Project page created: 07/09/2022.

Software used: Mathematica, Macaulay2

Project contributors: Philipp Dettling, Roser Homs, Carlos Améndola, Mathias Drton, Niels Richard Hansen.

Code written by: Philipp Dettling, Roser Homs, Carlos Améndola, Mathias Drton, Niels Richard Hansen.

Corresponding author of this page: Philipp Dettling, philipp.dettling@tum.de.
