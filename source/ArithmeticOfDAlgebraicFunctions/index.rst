===================================
Arithmetic of D-Algebraic Functions
===================================

| This page contains auxiliary files to the paper:
| Bertrand Teguia Tabuguia: Arithmetic of D-algebraic Functions
| ARXIV: https://arxiv.org/abs/2305.00702  CODE: https://mathrepo.mis.mpg.de/ArithmeticOfDAlgebraicFunctions

ABSTRACT: We are concerned with the arithmetic of solutions to ordinary or partial 
nonlinear differential equations which are algebraic in the indeterminates and their 
derivatives. We call these solutions D-algebraic functions, and their equations are 
algebraic (ordinary or partial) differential equations (ADEs). The general purpose is 
to find ADEs whose solutions are specified rational expressions of solutions to given ADEs. 
For univariate D-algebraic functions, we show how to derive an ADE whose order is bounded 
by the sum of the orders of the given algebraic ODEs. In the multivariate case, we prove 
that this cannot be done with algebraic PDEs, and introduce a general algorithm for these 
computations. Using our accompanying Maple software, we discuss applications in physics, 
statistics, and symbolic integration.


For instance, the square of the probability density function 

.. math:: \begin{equation*}
				f(x,\mu) := \frac{1}{\sigma \sqrt{2\,\pi}}\exp\left(-\frac{1}{2}\left(\frac{x-\mu}{\sigma}\right)^2\right),
			\end{equation*}

of the univariate `normal distribution <https://en.wikipedia.org/wiki/Normal_distribution>`__, 
seen as a bivariate function in the mean and the indeterminate variable, satisfies the following algebraic PDE (see Example 14 in the article)

.. math:: \begin{equation*}
				-z \! \left(x ,\mu \right) \left(\frac{\partial^{2}}{\partial \mu \partial x}z \! \left(x ,\mu \right)\right) \sigma^{2}+\left(\frac{\partial}{\partial \mu}z \! \left(x ,\mu \right)\right) \left(\frac{\partial}{\partial x}z \! \left(x ,\mu \right)\right) \sigma^{2}+2 z \! \left(x ,\mu \right)^{2}=0.
			\end{equation*}

This work comes as a complement of `D-Algebraic Functions <https://mathrepo.mis.mpg.de/DAlgebraicFunctions/index.html>`__ with an extension to the
arithmetic of multivariate D-algebraic functions. The  `NLDE package <https://mathrepo.mis.mpg.de/OperationsForDAlgebraicFunctions/index.html>`__ 
contains a subpackage called **MultiDalg** whose current main feature is **arithmeticMDalg** to work with algebraic PDEs. A version of the NLDE package
is directly made available here :download:`NLDE.mla <NLDE.mla>`. For the most recent version and source code visit 
`Github NLDE <https://github.com/T3gu1a/D-algebraic-functions>`__.

The following file is a Maple worksheet with the computations of the article. 

:download:`ArithmeticDalgebraicFunctions_MapleWorksheet.mw <ArithmeticDalgebraicFunctions_MapleWorksheet.mw>`.

The outputs are presented in the following printed pdf: 

:download:`Printed-PDF <ArithmeticDalgebraicFunctions_MapleWorksheet.pdf>`.

Project page created: 01/05/2023

Project contributors: Bertrand Teguia Tabuguia

Corresponding author of this page: Bertrand Teguia Tabuguia, teguia@mis.mpg.de

Software used: Maple 2022. 

The implementation works on recent versions of Maple: 2018 to 2023.

System setup used: Processor: Intel(R) Core(TM) i5-10210U CPU @ 1.60GHz 2.11 GHz, 
Installed RAM: 16,0 GB (15,8 GB usable), System type: 64-bit operating system, x64-based processor,
Edition: Windows 11 Home Version 21H2.
