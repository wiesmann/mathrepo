========================================
Twisted Cohomology and Likelihood Ideals
========================================

| This page contains auxiliary files to the paper:
| Saiei-Jaeyeong Matsubara-Heo and Simon Telen: Twisted Cohomology and Likelihood Ideals
| ARXIV: https://arxiv.org/abs/2301.13579 CODE: https://mathrepo.mis.mpg.de/TwistedCohomology

ABSTRACT: A likelihood function on a smooth very affine variety gives rise to a twisted de Rham complex. We show how its top cohomology vector space degenerates to the coordinate ring of the critical points defined by the likelihood ideal. We obtain a basis for cohomology from a basis of this coordinate ring. We investigate the dual picture, where twisted cycles correspond to critical points. We show how to expand a twisted cocycle in terms of a basis, and apply our methods to Feynman integrals from physics.

The following picture illustrates Lefschetz thimbles, which constitute a natural basis for twisted homology on the one dimensional torus with the third roots of unity removed.

.. image:: lefschetz.png
  :width: 500

This picture was generated using Mathematica. The script can be downloaded here: :download:`lefschetz.zip <lefschetz.zip>`.

We implemented our algorithms in `julia <https://julialang.org/>`_ (v1.8.3) using the packages `HomotopyContinuation.jl <https://www.juliahomotopycontinuation.org/>`_ (v2.6.4) and `Oscar.jl <https://juliapackages.com/p/polymake>`_ (v0.10.0). We include our implementation in this repository.

To download the code, click here :download:`code.zip <code.zip>`. The results of the computations reported in Section 6 can be found in txt format here :download:`results_Sec6.zip <results_Sec6.zip>`.

Project page created: 31/01/2023

Project contributors: Saiei-Jaeyeong Matsubara-Heo and Simon Telen

.. Jupyter notebook written by: Jane Doe, dd/mm/yy (this should be a comment and not displayed on the webpage)

Corresponding author of this page: Simon Telen, Simon.Telen@mis.mpg.de

Software used: Julia (Version 1.8.3), Mathematica (Version 12.3.1)
