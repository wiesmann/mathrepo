=======================================================================================================
Gaussian Process Priors for Systems of Linear Partial Differential Equations with Constant Coefficients
=======================================================================================================

| This page contains auxiliary files, animations, and demos connected to the paper:
| Marc Härkönen, Markus Lange-Hegermann, Bogdan Raiţă:
|   Gaussian Process Priors for Systems of Linear Partial Differential Equations with Constant Coefficients
| ARXIV: https://arxiv.org/abs/2212.14319
| CODE: https://github.com/haerski/EPGP


.. raw:: html

  <video width="45%" controls autoplay loop src="../_static/EPGP/wave_nice.mp4"></video>
  <video width="45%" controls autoplay loop src="../_static/EPGP/crashing_waves.mp4"></video>

Above, solutions to the 2D wave equation, solved using (S-)EPGP kernels with the first frame of the animation as initial data.
A demo using the S-EPGP kernel to learn a solution to Laplace's equation can be found in the `S-EPGP demo <demo.ipynb>`_ section.
The Jupyter notebook can be downloaded here: :download:`demo.ipynb <demo.ipynb>`.

.. toctree::
  :hidden:
  :maxdepth: 2

  demo


Generating solutions to Maxwell's equations
-------------------------------------------

We demonstrate the generative abilities of EPGP.
Below, we compute the posterior mean of the Gaussian process solving Maxwell's equations.
We condition on three initial frames containing a sprial in the electric field; no other initial data is given,
and the system is allowed to evolve by itself after that.
Below we plot field lines, electric field in blue and magnetic field in red.

.. raw:: html

  <video width="45%" controls autoplay loop src="../_static/EPGP/E.mp4"></video>
  <video width="45%" controls autoplay loop src="../_static/EPGP/B.mp4"></video>





2D Heat equation
-----------------

.. raw:: html

  <video width="45%" controls autoplay loop src="../_static/EPGP/melting_face_2.mp4"></video>
  <video width="45%" controls autoplay loop src="../_static/EPGP/melting_face_20.mp4"></video>

We apply our GP kernels to the 2D heat equation.
On the left, we use an EPGP kernel with scale parameter :math:`\sigma^2 = 2`, and on the right :math:`\sigma^2 = 20`.
Initial data is given at :math:`t = 0`, as a grid of values in :math:`\{0,1\}` in the shape of a smiling face.
In both cases, we see a gradual dissipation of heat over time.
The length scale affects how strongly the posterior mean attempts to fit the initial data.
Due to the instant smoothing property of the heat equation, the differences are visible only in the first frame of the animation.


Learning a 2D wave
-------------------

The animation below is a solution to a 2D wave equation, computed using a numerical solver.
It represents the vibration of a square membrane.

.. raw:: html

  <video width="45%" controls autoplay loop src="../_static/EPGP/true_wave.mp4"></video>

We try to learn the true solution using the first three frames of the above numerical solution as initial data.
With S-EPGP, we converge to a reasonable solution quickly.
Other than small oscillations on the boundary, the behavior of the learned wave is very close to the true solution.

.. raw:: html

  <video width="45%" controls autoplay loop src="../_static/EPGP/gp_wave.mp4"></video>

As a comparison, we also fit a Physics Informed Neutal Network (PINN) to the same data.
While the solution fits the initial data well (first three frames), it fails to extrapolate outside of the given training data.

.. raw:: html

  <video width="45%" controls autoplay loop src="../_static/EPGP/pinn_wave.mp4"></video>


Project page created: 06/01/2023

Project contributors: Marc Härkönen, Markus Lange-Hegermann, Bogdan Raiţă
 
Code written by: Marc Härkönen

Software used: Python (Version 3.9.15), PyTorch (Version 1.12.1), iPython (Version 8.5.0), Mathematica (Version 13), Macaulay2 (Version 1.20)

System setup used: Debian GNU/Linux 11 (bullseye), 2x 16-Core Intel Xeon Gold 6226R at 2.9 GHz, 384 GB RAM, Nvidia Tesla A100 80GB GPU.

Corresponding author of this page: Marc Härkönen, marc.harkonen@gmail.com

Last updated 09/01/2023.