=======================================================
Multiplicity structure of the arc space of a fat point
=======================================================

| This page contains auxiliary files to the paper:
| Rida Ait El Manssour, Gleb Pogudin: Multiplicity structure of the arc space of a fat point. 
| ARXIV: https://arxiv.org/abs/2111.10446 CODE: https://mathrepo.mis.mpg.de/MultiplicityStructureOfArcSpaces

   
Abstract:
Consider a fat point on a line, that is, the scheme defined by :math:`x^m = 0`. The algebra of regular functions on the arc space of this scheme can be represented as the quotient algebra of :math:`k[x, x', x^{(2)}, . . .]` by all the differential consequences of :math:`x^m = 0`. While this local algebra has infinite dimension, it admits a natural filtration by finite dimensional algebras corresponding to the truncations of arcs. We prove the generating series of the dimensions of these algebras is equal to :math:`\frac{m}{1 − mt}`. This result is motivated by nonreduced version of the geometric motivic Poincare ́ series, multiplicities in differential algebra, and recent connections between the arc spaces and the Rogers-Ramanujan identities. In particular, we prove a recent conjecture of Afsharijoo about the initial ideal of the ideal of the arc space of a fat point.


This repository includes the code discussed in Section 5.


Our code is written in `Macaulay2 <http://www2.macaulay2.com/Macaulay2/>`_ (v1.18):

:download:`code.m2 <code.m2>`


Project page created: 17/11/2021.

Project contributors: Rida Ait El Manssour, Gleb Pogudin.

Corresponding author of this page: Rida Ait El Manssour, rida.manssour@mis.mpg.de

Software used: Macaulay2 (v1.18).

System setup used: MacBook Pro with macOS Monterey 12.0.1, Processor 3,3 GHz Intel Core i5, Memory 16 GB 2133 MHz LPDDR3, Graphics Intel Iris Graphics 550 1536 MB.
