needsPackage "Matroids"
needsPackage "Graphs"

-- This function creates a ring whose variables correspond to the columns of the Jacobian of the SEM
-- n, an interger representing the number of vertices of the underlying graph
concRing = n -> QQ[apply(n, i -> k_{i, i}) | apply(subsets(n, 2), i -> k_{i_0, i_1})]

-- This function creates a ring whose variables correspond to the rows of the Jacobian matrices of the SEM
-- G, a digraph
lRing = G -> (

  V := sort(vertices(G));

  return QQ[apply(edges(G), e -> l_e) | {s}];
  )

-- This function creates the parameterization of the precision matrix of the linear SEM on G
-- G, a digraph
gaussPrecParam = {paramRing => null} >> opts -> G -> (

  n := #vertices(G);
  S := if opts.paramRing === null then lRing(G) else opts.paramRing;
  use S;

  L := (0_S)*mutableIdentity(S, n);
  for e in edges(G) do L_(toSequence(e)) = l_e;
  L = matrix L;
  I := id_(S^n);
  phi := s*(I-L)*(transpose(I-L));
 

  return apply(subsets(n,2), i -> phi_(toSequence i));
  )

-- This function creates the jacobian parameterization of the precision matrix of the linear SEM on G
-- G, a digraph
gaussPrecJac = G -> jacobian matrix {gaussPrecParam(G)}


-- This function creates all possible complete digraphs on n vertices
-- n, the number of vertices for the complete graphs
generateCyclicGraphs = n -> (


	undirectedEdges = subsets(n, 2);

	graphList = for dir in toList(binomial(n,2)-1:0)..toList(binomial(n,2)-1:1) list(

		dir = dir | {0};

		digraph apply(undirectedEdges, dir, (e, i) -> if i == 0 then e else reverse e)
		);

	return graphList
	)


-- This loop checks that reversing the edge between (n-1) and n yields the same matroid
-- We simply check using the matroids package for every graph
-- Simply change n and run the whole file to verify the success for other values of n
-- Note that n = 6 takes about 30 minutes to run
n = 6

matroidTests = for G in generateCyclicGraphs(n) list(

	H = if member({n-2, n-1}, edges(G)) then H = addEdges'(deleteEdges(G, {{n-2, n-1}}), {{n-1, n-2}}) else addEdges'(deleteEdges(G, {{n-1, n-2}}), {{n-2, n-1}});

	MG = matroid(gaussPrecJac(G));
	MH = matroid(gaussPrecJac(H));

	MG == MH
	)

-- This simply checks every result in the previous list was true by
-- converting the boolean outcomes to integers and then checking the product is 1, meaning they were all true
-- If the following value is 1 then the conjecture is true for that n
checkAllPairsAreEqual = product apply(matroidTests, i -> if i then 1 else 0)
