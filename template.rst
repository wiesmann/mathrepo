* This is a .rst file - please see the .rst documentation for details. Quick reference: https://docutils.sourceforge.io/docs/user/rst/quickref.html or refer to the other projects in this repository as examples.
* All instructions are marked with a *

======
Header
======

Subsection Header
-----------------

* Make sure the === and the --- match the length of your header and subheader.

* State the following on top of the page: Page content type (e. g. supplementary code); related publication: authors, title, arXiv-ID and DOI (if applicable); For example:

| This page contains auxiliary files to the paper:
| Mohamed Barakat, Reimer Behrends, Christoper Jefferson, Lukas Kühne, and Martin Leuner: On the generation of rank 3 simple matroids with an application to Terao' freeness conjecture
| MIS-Preprint: `88/2020 <https://www.mis.mpg.de/publications/preprints/2020/prepr2020-88.html>`_ ARXIV: https://arxiv.org/abs/1907.01073 CODE: https://mathrepo.mis.mpg.de/GenerationOfMatroids


* Please add a short summary as a general explanation of the project (about 2 - 4 sentences).

* Subsequently, you can briefly describe the purpose and structure of this mathrepo side in a couple of sentences.


* A picture can be included like this:

.. image:: triangle.png

* Please also add a sentence as a caption below the picture briefly explaning it.



* A link can be placed like this:
 `https://www.mis.mpg.de <https://www.mis.mpg.de>`_.

* If applicable:
The code is presented as a Jupyter notebook in Julia which can be downloaded here: download:`my_notebook.ipynb <my_notebook.ipynb>`.

* Add a table of contents (built from the type of heading you use in the respective files) and definition of subpages. Note that each subpage needs a separate .rst file:

.. toctree::
	:maxdepth: 2

  subpage1.rst
  subpage2.rst
  subpage3.rst
  my_notebook_without_file_ending
  


* LaTex Example:
For :math:`n\ge 1`:

.. math::
  \sum_{i=1}^n i = \frac{n(n+1)}{2}





* A codeblock example in 
:math:`\verb|Macaulay2|`:

.. code-block:: macaulay2

		K=QQ;
		S=frac(K[l0,l1,l2,m]);
		R=S[u]; -- Some comment can go here.




* Add other downloadable files like this:

The source code or auxiliary files can be downloaded here: :download:`some_code.m2 <some_code.m2>`.

* A comment can be written like this (it will not be visible on the website):
.. This is a comment.
------------------------------------------------------------------------------------------------------------------------------
* Please always indicate the following:

Project page created: dd/mm/yyyy

Project contributors: Jane Doe, John Doe

Corresponding author of this page: Jane Doe, janedoe@mis.mpg.de

* Responsible person for project page on https://mathrepo.mis.mpg.de (optionally with persistent mail address):
 
Code written by: Jane Doe, dd/mm/yyyy

* (if not identical with project contributors, also with date if differing from the publication's date)

Jupyter notebook written by: Jane Doe, dd/mm/yy

Software used: Julia (Version 1.5.2), Macaulay2 (v1.20)

System setup used: MacBook Pro with macOS ..., Processor ..., Memory ..., Graphics ... .

* If applicable, please include all relevant information regarding the code: Version and system setup you used for computing your results in the corresponding paper.

License for code of this project page: MIT License (https://spdx.org/licenses/MIT.html)

* If you prefer another licence, you can use that instead. Just make sure to state it clearly with a link to the license text (see https://spdx.org/licenses)

License for all other content of this project page (text, images, …): CC BY 4.0 (https://creativecommons.org/licenses/by/4.0/)


Last updated dd/mm/yyyy.