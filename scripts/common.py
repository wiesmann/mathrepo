#!/usr/bin/env python3

import os
import re

from termcolors import colors

# known software
SOFTWARE = { 'gap'         : 'GAP',
             'julia'       : 'Julia',
             'macaulay'    : 'Macaulay2',
             'magma'       : 'MAGMA',
             'maple'       : 'Maple',
             'mathematica' : 'Mathematica',
             'matlab'      : 'MATLAB',
             'oscar'       : 'OSCAR',
             'perl'        : 'Perl',
             'polymake'    : 'Polymake',
             'python'      : 'Python',
             'sage'        : 'SageMath',
             'sagemath'    : 'SageMath',
             'singular'    : 'Singular' }

#
# return list of all index.rst files
#
def get_indexdb () :
    indexdb = []
    
    for root, dirs, files in os.walk( '.' ) :
        for filename in files :
            if filename == 'index.rst' and root != './source' :
                indexdb.append( os.path.join( root, filename ) )

    return indexdb

#
# build database of index files
#
# - maps filenames to title, author list, software list
#
def build_indexdb () :
    # search for index.rst files
    index_files = get_indexdb()
        
    #
    # generate DB
    #

    # things to remove
    author_rem_re   = re.compile( '\(\w*\)|:raw-html:`[\w<>/ ]+`' )
    software_rem_re = re.compile( '\(.*?\)|:raw-html:`[\w<>/ ]+`|[ \.\n]+$|^ +|(\d+\.)*\d+\.?|-dev|version', re.IGNORECASE )
    
    DB = {}

    for filename in index_files :

        title    = None
        authors  = []
        software = []

        with open( filename, 'r' ) as index :

            #
            # first read title:
            # - enclosed between two '======...' lines 
            #

            read_title = False
            for line in index :

                if line.find( '=====' ) == 0 :

                    if read_title :
                        # reached end of title
                        break
                    
                    read_title = True
                    continue

                if read_title :
                    title = line.lstrip().rstrip()
            
            #
            # look for authors and software
            #
            
            for line in index :

                #
                # authors
                #
                
                pos = line.find( 'Project contributors:' )
                if pos >= 0 :
                    line       = line[pos+21:]
                    line       = line.replace( ' and ', ',' )
                    authorlist = line.split( ',' )

                    #
                    # clean up author list
                    #
        
                    for author in authorlist :
                        res = author_rem_re.search( author )
                        while res != None :
                            author = author.replace( res.group(), '' )
                            res    = author_rem_re.search( author )
                    
                        author = author.lstrip().rstrip()
                        author = author.rstrip( '.' )

                        if author != '' :
                            if not author in authors :
                                authors.append( author )

                #
                # software
                #

                pos = line.find( 'Software used:' )
                if pos >= 0 :
                    line = line[pos+15:]
                    line = line.replace( ' and ', ',' )

                    # remove all known things like version, etc.
                    res  = software_rem_re.search( line )
                    while res != None :
                        line = line.replace( res.group(), '' )
                        res  = software_rem_re.search( line )

                    software_list = line.split( ',' )

                    for sw in software_list :
                        # just in case we have whitespaces left
                        sw = sw.lstrip().rstrip()

                        if sw != '' :
                            # replace software by correct name
                            if sw.lower() in SOFTWARE :
                                sw = SOFTWARE[sw.lower()]
                            else :
                                print( '{}info{}: unknown software "{}" found in {}'.format( colors['green'], colors['reset'], sw, filename ) )

                            if not sw in software :
                                software.append( sw )
                
                
        #
        # finish entry
        #

        if title == None :
            print( '{}warning{}: no title found in {}'.format( colors['yellow'], colors['reset'], filename ) )
            
        if len(authors) == 0 :
            print( '{}warning{}: no authors found in {}'.format( colors['yellow'], colors['reset'], filename ) )
                            
        if len(software) == 0 :
            print( '{}warning{}: no authors found in {}'.format( colors['yellow'], colors['reset'], filename ) )
                            
        DB[filename] = { 'title'    : title,
                         'authors'  : authors,
                         'software' : software }

    # finally return database
    return DB

#
# return title without leading a/the
#
def canonical_title ( title ) :
    if title.find( 'a ' )   == 0 : title = title[2:]
    if title.find( 'A ' )   == 0 : title = title[2:]
    if title.find( 'an ' )  == 0 : title = title[3:]
    if title.find( 'An ' )  == 0 : title = title[3:]
    if title.find( 'the ' ) == 0 : title = title[4:]
    if title.find( 'The ' ) == 0 : title = title[4:]

    return title

