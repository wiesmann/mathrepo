#!/usr/bin/env python3
#
# initialize and provide terminal colors
#

import sys, os

colors = { 'reset'  : '\033[0m',
           'bold'   : '\033[1m',
           'italic' : '\033[3m',
           'red'    : '\033[31m',
           'green'  : '\033[32m',
           'yellow' : '\033[33m',
           'blue'   : '\033[34m',
           'purple' : '\033[35m',
           'cyan'   : '\033[36m',
           'gray'   : '\033[37m' }

# no colors if wanted or output is not a terminal ('dumb' is for emacs)
if not sys.stdout.isatty() or os.environ['TERM'] == 'dumb' :
    for key in colors.keys() :
        colors[key] = ''
else :
    # try to handle above codes on non-supported systems
    try:
        import colorama

        colorama.init()
    except :
        pass
